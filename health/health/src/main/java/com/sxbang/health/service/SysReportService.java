package com.sxbang.health.service;

import com.sxbang.health.base.result.Results;
import com.sxbang.health.model.SysReport;

import java.util.ArrayList;
import java.util.Map;

public interface SysReportService {

    Map<String, ArrayList> getDictSet();

    Results save(SysReport report);
}
