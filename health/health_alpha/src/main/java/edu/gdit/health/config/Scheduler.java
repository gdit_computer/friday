package edu.gdit.health.config;


import edu.gdit.health.service.SysReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 定时统计数据采集信息
 */
@Component
public class Scheduler {

    @Autowired
    private SysReportService sysReportService;

    @Scheduled(fixedRate = 600000)
    public void countReportData() {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        System.out.println("------ 定时统计执行时间：" + sdf.format(now));
        sysReportService.countReportData(now);
    }
}
