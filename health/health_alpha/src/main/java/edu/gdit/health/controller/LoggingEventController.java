package edu.gdit.health.controller;


import edu.gdit.health.base.result.PageTableRequest;
import edu.gdit.health.base.result.Results;
import edu.gdit.health.service.LoggingEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日志事件控制层
 */
@Controller
@RequestMapping("/log")
public class LoggingEventController {

    /**
     * 注入日志事件服务
     */
    @Autowired
    private LoggingEventService loggingEventService;

    /**
     * 获取日志列表
     * @param request 分页请求参数
     * @return Results 返回分页结果集对象
     */
    @GetMapping("/list")
    @ResponseBody
    public Results list(PageTableRequest request) {
        request.countOffset();
        return loggingEventService.getListByPage(request.getOffset(), request.getLimit());
    }

    //Date日期格式
    String pattern = "yyyy-MM-dd";
    //为当前控制器注册一个Date日期类型属性编辑器
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));// CustomDateEditor为自定义日期编辑器
    }
}
