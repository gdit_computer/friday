-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: gdit_health
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `logging_event`
--

DROP TABLE IF EXISTS `logging_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logging_event` (
  `timestmp` bigint NOT NULL,
  `formatted_message` text NOT NULL,
  `logger_name` varchar(254) NOT NULL,
  `level_string` varchar(254) NOT NULL,
  `thread_name` varchar(254) DEFAULT NULL,
  `reference_flag` smallint DEFAULT NULL,
  `arg0` varchar(254) DEFAULT NULL,
  `arg1` varchar(254) DEFAULT NULL,
  `arg2` varchar(254) DEFAULT NULL,
  `arg3` varchar(254) DEFAULT NULL,
  `caller_filename` varchar(254) NOT NULL,
  `caller_class` varchar(254) NOT NULL,
  `caller_method` varchar(254) NOT NULL,
  `caller_line` char(4) NOT NULL,
  `event_id` bigint NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`event_id`)
) ENGINE=InnoDB AUTO_INCREMENT=698 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logging_event`
--

LOCK TABLES `logging_event` WRITE;
/*!40000 ALTER TABLE `logging_event` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logging_event_exception`
--

DROP TABLE IF EXISTS `logging_event_exception`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logging_event_exception` (
  `event_id` bigint NOT NULL,
  `i` smallint NOT NULL,
  `trace_line` varchar(254) NOT NULL,
  PRIMARY KEY (`event_id`,`i`),
  CONSTRAINT `logging_event_exception_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `logging_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logging_event_exception`
--

LOCK TABLES `logging_event_exception` WRITE;
/*!40000 ALTER TABLE `logging_event_exception` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging_event_exception` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logging_event_property`
--

DROP TABLE IF EXISTS `logging_event_property`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `logging_event_property` (
  `event_id` bigint NOT NULL,
  `mapped_key` varchar(140) NOT NULL,
  `mapped_value` text,
  PRIMARY KEY (`event_id`,`mapped_key`),
  CONSTRAINT `logging_event_property_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `logging_event` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logging_event_property`
--

LOCK TABLES `logging_event_property` WRITE;
/*!40000 ALTER TABLE `logging_event_property` DISABLE KEYS */;
/*!40000 ALTER TABLE `logging_event_property` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_permission`
--

DROP TABLE IF EXISTS `sys_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_permission` (
  `id` int NOT NULL AUTO_INCREMENT,
  `parentId` int NOT NULL,
  `name` varchar(50) NOT NULL,
  `css` varchar(30) DEFAULT NULL,
  `href` varchar(1000) DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `permission` varchar(50) DEFAULT NULL,
  `sort` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_permission`
--

LOCK TABLES `sys_permission` WRITE;
/*!40000 ALTER TABLE `sys_permission` DISABLE KEYS */;
INSERT INTO `sys_permission` VALUES (1,0,'用户管理','fa-users','',1,'',1),(2,1,'用户查询','fa-user','/api/getPage?pageName=user/user-list',1,'',2),(3,2,'查询','','',2,'sys:user:query',100),(4,2,'新增','','',2,'sys:user:add',100),(5,2,'删除',NULL,NULL,2,'sys:user:del',100),(6,1,'修改密码','fa-pencil-square-o','/api/getPage?pageName=user/user-change-password',1,'sys:user:password',4),(7,0,'系统设置','fa-gears','',1,'',5),(8,7,'菜单','fa-cog','/api/getPage?pageName=permission/permission-list',1,'',6),(9,8,'查询','','',2,'sys:menu:query',100),(10,8,'新增','','',2,'sys:menu:add',100),(11,8,'删除','','',2,'sys:menu:del',100),(12,7,'角色','fa-user-secret','/api/getPage?pageName=role/role-list',1,'',7),(13,12,'查询','','',2,'sys:role:query',100),(14,12,'新增','','',2,'sys:role:add',100),(15,12,'删除','','',2,'sys:role:del',100),(19,0,'数据源监控','fa-eye','druid/index.html',1,'',9),(22,0,'日志查询','fa-reorder','/api/getPage?pageName=log/list',1,'sys:log:query',13),(23,8,'修改',NULL,NULL,2,'sys:menu:edit',100),(24,12,'修改',NULL,NULL,2,'sys:role:edit',100),(25,2,'修改',NULL,NULL,2,'sys:user:edit',100),(53,0,'数据填报','','',1,'',1),(54,53,'数据填报','','/report/add',1,'sys:report:add',100),(55,53,'填报记录','','/api/getPage?pageName=home/report-log',1,'',100),(56,0,'数据监控','','',1,'',2),(57,56,'数据监控','','/api/getPage?pageName=report/data',1,'sys:data:page',101);
/*!40000 ALTER TABLE `sys_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_report`
--

DROP TABLE IF EXISTS `sys_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_report` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `createTime` datetime NOT NULL COMMENT '  上报时间',
  `user_id` int DEFAULT NULL COMMENT '上报用户',
  `remark` char(64) DEFAULT NULL COMMENT '注释',
  `temperature` varchar(20) DEFAULT NULL COMMENT '体温',
  `travel` tinyint(1) DEFAULT '0' COMMENT '近期是否有旅游，0=没有，1=有',
  `travel_description` varchar(255) DEFAULT NULL COMMENT '近期行程描述',
  `physical_condition` varchar(255) DEFAULT NULL COMMENT '近日身体状况',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_report`
--

LOCK TABLES `sys_report` WRITE;
/*!40000 ALTER TABLE `sys_report` DISABLE KEYS */;
INSERT INTO `sys_report` VALUES (8,'2021-01-09 19:28:47',1,NULL,'NORMAL',0,'123','travel,normal,contact'),(9,'2021-01-09 20:01:56',1,NULL,'LOW',1,'阿斯顿','travel,normal'),(10,'2021-01-12 16:40:39',1,NULL,'NORMAL',0,'测试用数据','travel,contact,normal,rheum,polypnea,vomiting,flustered,conjunctivitis'),(11,'2021-01-13 09:32:36',1,NULL,'NORMAL',0,'','travel,contact'),(12,'2021-01-14 09:32:31',1,NULL,'NORMAL',0,'无','normal,rheum'),(13,'2021-01-14 09:33:00',1,NULL,'LOW',0,'珠海 至 中山','travel,contact'),(14,'2021-01-14 09:33:02',1,NULL,'LOW',0,'珠海 至 中山 ','travel,contact'),(15,'2021-01-15 17:06:57',1,NULL,'NORMAL',0,'无','normal'),(16,'2021-01-19 09:39:18',1,NULL,'NORMAL',0,'','travel,contact'),(17,'2021-01-19 09:40:16',1,NULL,'NORMAL',0,'','contact,normal'),(18,'2021-01-19 10:16:09',1,NULL,'NORMAL',0,'','normal'),(19,'2021-01-19 11:03:02',1,NULL,'LOW',0,'无','travel,rheum'),(20,'2021-01-19 11:11:41',1,NULL,NULL,0,'','travel'),(21,'2021-01-19 11:16:06',1,NULL,'LOW',0,'去湖南长沙探亲','travel,normal,polypnea'),(22,'2021-01-19 11:18:44',1,NULL,'LOW',1,'去湖南长沙','travel,contact,flustered'),(23,'2021-01-19 11:19:48',1,NULL,'NORMAL',1,'呆在珠海','travel,normal'),(24,'2021-01-19 11:22:27',1,NULL,'NORMAL',0,'呆在珠海','travel,contact'),(25,'2021-01-19 11:24:03',1,NULL,'NORMAL',1,'呆在珠海','contact,normal'),(27,'2021-01-19 11:36:36',1,NULL,'NORMAL',0,'呆在珠海','contact,normal'),(28,'2021-01-19 11:37:39',1,NULL,'NORMAL',1,'呆在珠海','normal'),(29,'2021-01-19 11:39:38',1,NULL,'NORMAL',0,'呆在珠海','contact,normal'),(30,'2021-01-19 11:41:39',1,NULL,'NORMAL',0,'呆在珠海','contact,normal'),(31,'2021-01-19 14:25:42',1,NULL,'NORMAL',0,'呆在珠海','contact,normal'),(32,'2021-01-19 14:28:46',1,NULL,'NORMAL',0,'呆在珠海','contact,normal'),(33,'2021-01-19 14:29:53',1,NULL,'NORMAL',0,'呆在珠海','contact,normal'),(34,'2021-01-19 14:30:41',1,NULL,'NORMAL',0,'呆在珠海','contact,normal');
/*!40000 ALTER TABLE `sys_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_report_count`
--

DROP TABLE IF EXISTS `sys_report_count`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_report_count` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `report_date` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31982 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_report_count`
--

LOCK TABLES `sys_report_count` WRITE;
/*!40000 ALTER TABLE `sys_report_count` DISABLE KEYS */;
INSERT INTO `sys_report_count` VALUES (1585,'2021-01-13','2','count','2021-01-13 09:48:05','2021-01-13'),(5966,'LOW','2','temperature','2021-01-14 15:59:17','2021-01-14'),(5967,'NORMAL','1','temperature','2021-01-14 15:59:17','2021-01-14'),(5968,'0','3','travel','2021-01-14 15:59:17','2021-01-14'),(5969,'2021-01-14','3','count','2021-01-14 15:59:17','2021-01-14'),(5970,'rheum','1','physical_condition','2021-01-14 15:59:17','2021-01-14'),(5971,'normal','1','physical_condition','2021-01-14 15:59:17','2021-01-14'),(5972,'polypnea','0','physical_condition','2021-01-14 15:59:17','2021-01-14'),(5973,'contact','2','physical_condition','2021-01-14 15:59:17','2021-01-14'),(5974,'flustered','0','physical_condition','2021-01-14 15:59:17','2021-01-14'),(5975,'travel','2','physical_condition','2021-01-14 15:59:17','2021-01-14'),(5976,'conjunctivitis','0','physical_condition','2021-01-14 15:59:17','2021-01-14'),(5977,'vomiting','0','physical_condition','2021-01-14 15:59:17','2021-01-14'),(10532,'NORMAL','1','temperature','2021-01-15 15:59:57','2021-01-15'),(10533,'0','1','travel','2021-01-15 15:59:57','2021-01-15'),(10534,'2021-01-15','1','count','2021-01-15 15:59:57','2021-01-15'),(10535,'rheum','0','physical_condition','2021-01-15 15:59:57','2021-01-15'),(10536,'normal','1','physical_condition','2021-01-15 15:59:57','2021-01-15'),(10537,'polypnea','0','physical_condition','2021-01-15 15:59:57','2021-01-15'),(10538,'contact','0','physical_condition','2021-01-15 15:59:57','2021-01-15'),(10539,'flustered','0','physical_condition','2021-01-15 15:59:57','2021-01-15'),(10540,'travel','0','physical_condition','2021-01-15 15:59:57','2021-01-15'),(10541,'conjunctivitis','0','physical_condition','2021-01-15 15:59:57','2021-01-15'),(10542,'vomiting','0','physical_condition','2021-01-15 15:59:57','2021-01-15'),(11676,'NORMAL','1','temperature','2021-01-22 08:50:12','2021-01-22'),(11677,'0','1','travel','2021-01-22 08:50:12','2021-01-22'),(16395,'LOW','1','temperature','2021-01-22 15:59:12','2021-01-22'),(16396,'1','1','travel','2021-01-22 15:59:12','2021-01-22'),(16397,'2021-01-22','1','count','2021-01-22 15:59:12','2021-01-22'),(16398,'rheum','0','physical_condition','2021-01-22 15:59:12','2021-01-22'),(16399,'normal','0','physical_condition','2021-01-22 15:59:12','2021-01-22'),(16400,'polypnea','0','physical_condition','2021-01-22 15:59:12','2021-01-22'),(16401,'contact','1','physical_condition','2021-01-22 15:59:12','2021-01-22'),(16402,'flustered','0','physical_condition','2021-01-22 15:59:12','2021-01-22'),(16403,'travel','0','physical_condition','2021-01-22 15:59:12','2021-01-22'),(16404,'conjunctivitis','0','physical_condition','2021-01-22 15:59:12','2021-01-22'),(16405,'vomiting','0','physical_condition','2021-01-22 15:59:12','2021-01-22'),(31971,'NORMAL','1','temperature','2021-01-25 15:59:12','2021-01-25'),(31972,'1','1','travel','2021-01-25 15:59:12','2021-01-25'),(31973,'2021-01-25','1','count','2021-01-25 15:59:12','2021-01-25'),(31974,'rheum','0','physical_condition','2021-01-25 15:59:12','2021-01-25'),(31975,'normal','0','physical_condition','2021-01-25 15:59:12','2021-01-25'),(31976,'polypnea','0','physical_condition','2021-01-25 15:59:12','2021-01-25'),(31977,'contact','1','physical_condition','2021-01-25 15:59:12','2021-01-25'),(31978,'flustered','0','physical_condition','2021-01-25 15:59:12','2021-01-25'),(31979,'travel','1','physical_condition','2021-01-25 15:59:12','2021-01-25'),(31980,'conjunctivitis','1','physical_condition','2021-01-25 15:59:12','2021-01-25'),(31981,'vomiting','0','physical_condition','2021-01-25 15:59:12','2021-01-25');
/*!40000 ALTER TABLE `sys_report_count` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role`
--

DROP TABLE IF EXISTS `sys_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `description` varchar(100) DEFAULT NULL,
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role`
--

LOCK TABLES `sys_role` WRITE;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` VALUES (1,'ADMIN','管理员','2017-05-01 13:25:39','2021-01-21 01:33:20'),(2,'USER','普通用户','2017-08-01 21:47:31','2021-01-06 20:58:03'),(3,'TEACHER','教师','2019-03-27 02:10:23','2021-01-06 21:03:07'),(4,'test','test','2019-04-29 02:16:48','2019-05-22 09:51:26'),(16,'SLeader','校长','2021-01-07 10:40:17','2021-01-07 10:40:17'),(17,'YLeader','院长','2021-01-07 10:40:44','2021-01-07 10:40:44'),(18,'STUDENT','学生','2021-01-07 10:54:38','2021-01-06 23:25:00'),(30,'测试角色','测试角色','2021-01-08 16:05:09','2021-01-08 16:05:09');
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_permission`
--

DROP TABLE IF EXISTS `sys_role_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_permission` (
  `roleId` int NOT NULL,
  `permissionId` int NOT NULL,
  PRIMARY KEY (`roleId`,`permissionId`),
  KEY `fk_sysrolepermission_permissionId` (`permissionId`),
  CONSTRAINT `fk_permission_roleId` FOREIGN KEY (`roleId`) REFERENCES `sys_role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_sysrolepermission_permissionId` FOREIGN KEY (`permissionId`) REFERENCES `sys_permission` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_permission`
--

LOCK TABLES `sys_role_permission` WRITE;
/*!40000 ALTER TABLE `sys_role_permission` DISABLE KEYS */;
INSERT INTO `sys_role_permission` VALUES (1,1),(2,1),(3,1),(4,1),(16,1),(17,1),(30,1),(1,2),(2,2),(3,2),(4,2),(16,2),(17,2),(30,2),(1,3),(2,3),(3,3),(4,3),(16,3),(17,3),(30,3),(1,4),(4,4),(1,5),(4,5),(1,6),(2,6),(3,6),(4,6),(1,7),(3,7),(4,7),(16,7),(17,7),(1,8),(3,8),(4,8),(16,8),(17,8),(1,9),(3,9),(4,9),(16,9),(17,9),(1,10),(4,10),(1,11),(4,11),(1,12),(4,12),(16,12),(1,13),(4,13),(16,13),(1,14),(4,14),(1,15),(4,15),(1,19),(3,19),(4,19),(1,22),(4,22),(1,23),(4,23),(1,24),(4,24),(1,25),(4,25),(1,53),(2,53),(3,53),(18,53),(1,54),(2,54),(3,54),(18,54),(1,55),(2,55),(3,55),(18,55),(1,56),(2,56),(3,56),(1,57),(3,57);
/*!40000 ALTER TABLE `sys_role_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_role_user`
--

DROP TABLE IF EXISTS `sys_role_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_role_user` (
  `userId` int NOT NULL,
  `roleId` int NOT NULL,
  PRIMARY KEY (`userId`,`roleId`),
  KEY `fk_roleId` (`roleId`),
  CONSTRAINT `fk_roleId` FOREIGN KEY (`roleId`) REFERENCES `sys_role` (`id`),
  CONSTRAINT `fk_userId` FOREIGN KEY (`userId`) REFERENCES `sys_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_role_user`
--

LOCK TABLES `sys_role_user` WRITE;
/*!40000 ALTER TABLE `sys_role_user` DISABLE KEYS */;
INSERT INTO `sys_role_user` VALUES (1,1),(2,2),(18,2),(27,2),(28,2),(29,2),(30,2),(41,2),(3,3),(26,3),(32,3),(46,3),(45,18);
/*!40000 ALTER TABLE `sys_role_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_user`
--

DROP TABLE IF EXISTS `sys_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sys_user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(60) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `headImgUrl` varchar(255) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `telephone` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `createTime` datetime NOT NULL,
  `updateTime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_user`
--

LOCK TABLES `sys_user` WRITE;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` VALUES (1,'admin','$2a$10$DFIwAy//Ol3X6Q1e5CEue.FfUnJ5Fj709z9oY1pwCWzpca.SpYs72','管理员',NULL,NULL,'158784879852','12@qq.com',NULL,NULL,1,'2019-04-08 00:20:51','2019-04-08 00:20:51'),(2,'user','$2a$10$ILWAB4ZOoRr2pXqarliI6uAuL7Q/7dAMTpWO9p7dyVSHHO7zQMTeW','用户',NULL,NULL,'1111111111','11@qq.com','2019-03-30',1,1,'2019-04-09 06:44:50','2021-01-06 20:51:33'),(3,'alex','534b44a19bf18d20b71ecc4eb77c572f','讲师',NULL,'','13245698712','alex@qq.com','2019-03-31',1,1,'2019-03-27 02:27:35','2019-04-09 07:57:17'),(18,'user1','96e79218965eb72c92a549dd5a330112','111',NULL,NULL,'123455432123','134@qq.com','2019-05-12',NULL,1,'2019-05-14 04:44:22','2019-05-14 04:44:22'),(26,'user2','96e79218965eb72c92a549dd5a330112','user2',NULL,NULL,'09876567890','aa@QQ.com','2019-05-12',NULL,1,'2019-05-15 02:22:21','2019-05-21 00:57:14'),(27,'user3','96e79218965eb72c92a549dd5a330112','user3',NULL,NULL,'44366758876586578','bb@qq.com','2019-05-14',NULL,1,'2019-05-15 02:23:51','2019-05-15 02:23:51'),(28,'user4','96e79218965eb72c92a549dd5a330112','user4',NULL,NULL,'2143323543456876','cc@qq.com','2019-04-30',NULL,1,'2019-05-15 02:24:22','2019-05-15 02:24:22'),(29,'user5','96e79218965eb72c92a549dd5a330112','user5',NULL,NULL,'1221344234565','dd@qq.com','2018-12-03',NULL,1,'2019-05-15 02:24:49','2019-05-15 02:24:49'),(30,'user6','96e79218965eb72c92a549dd5a330112','user6',NULL,NULL,'123213215135453','ee@qq.coom','2019-05-15',NULL,1,'2019-05-15 02:25:16','2019-05-21 03:08:26'),(32,'user7','96e79218965eb72c92a549dd5a330112','user7',NULL,NULL,'21345457980765','tt@qq.com','2019-05-20',NULL,1,'2019-05-15 06:16:32','2019-05-21 03:08:37'),(41,'user67','96e79218965eb72c92a549dd5a330112','user67',NULL,NULL,'123456324568','asdsa@qq.com','2019-05-14',NULL,1,'2019-05-16 08:39:11','2019-05-16 08:39:11'),(43,'alex-s','$2a$10$uO3EmAB8LgkreKnwiro1Ium2n28iHDJw66e4prCesSOu0NrNvmkhu',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,'2019-05-29 06:07:53','2019-05-29 06:07:53'),(45,'student1','$2a$10$IfK4TtYXUnMDBEM5mKTzhO.l3dE/EsKH0YpPjrBVvtQ8SUWAmprj.','学生1',NULL,NULL,'12345678912','student1@test.com',NULL,1,1,'2021-01-07 10:59:32','2021-01-07 10:59:32'),(46,'teacher1','$2a$10$nCHZ2R2cuJ.0MsjZfMMqT.XO2BOLpCsZsZ.rpwSkQUONI6z7bVwGO','教师1',NULL,NULL,'12345678913','teacher1@test.com',NULL,1,1,'2021-01-07 11:00:16','2021-01-07 11:00:16');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'gdit_health'
--

--
-- Dumping routines for database 'gdit_health'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-10 17:46:55
