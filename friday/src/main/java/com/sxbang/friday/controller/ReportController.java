package com.sxbang.friday.controller;

import com.sxbang.friday.base.result.PageTableRequest;
import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.model.SysReport;
import com.sxbang.friday.service.DictService;
import com.sxbang.friday.service.ReportService;
import com.sxbang.friday.util.SecurityUtil;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.*;

@Controller
@RequestMapping("report")
@Slf4j
public class ReportController {

	@Autowired
	private ReportService reportService;

    @Autowired
    private DictService dictService;

/*	@GetMapping("/all")
	@ResponseBody
    @ApiOperation(value = "获取所有角色", notes = "获取所有角色信息")//描述
	public Results<SysReport> getAll() {
		return reportService.getAllReports();
	}*/

/*	@GetMapping("/list")
	@ResponseBody
    @PreAuthorize("hasAuthority('sys:report:query')")
    @ApiOperation(value = "分页获取角色", notes = "用户分页获取角色信息")//描述
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "limit", required = true,dataType = "Integer"),
    })
	public Results list(PageTableRequest request) {
		log.info("ReportController.list(): param ( request = " + request +" )");
		request.countOffset();
		return reportService.getAllReportsByPage(request.getOffset(), request.getLimit());
	}*/

    @GetMapping("self/list")
	@ResponseBody
    @PreAuthorize("hasAuthority('sys:report:add')")
    @ApiOperation(value = "分页获取角色", notes = "用户分页获取角色信息")//描述
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "limit", required = true,dataType = "Integer"),
    })
	public Results selfList(PageTableRequest request) {
		log.info("ReportController.selfList(): param ( request = " + request +" )");
		request.countOffset();
//		return reportService.getAllReportsByPage(request.getOffset(), request.getLimit());
        return reportService.getSelfReportsByPage(request.getOffset(), request.getLimit());
	}

    @GetMapping(value = "/add")
    @PreAuthorize("hasAuthority('sys:report:add')")
    @ApiOperation(value = "新增角色信息页面", notes = "跳转到角色信息新增页面")//描述
    public String addReport(Model model) {
        log.warn(String.format("用户 %s 进入数据采集页面。", SecurityUtil.getLoginUser().getUsername()));
        model.addAttribute("sysReport",new SysReport());
        Map<String, ArrayList> dict = dictService.getDictSet(new String[]{"temperature" ,"travel" ,"physical_condition" });
        model.addAttribute("reportDict", dict);
        return "home/report";
    }

    @PostMapping(value = "/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:report:add')")
    @ApiOperation(value = "保存角色信息", notes = "保存新增的角色信息")//描述
    @ApiImplicitParam(name = "reportDto",value = "角色信息实体类", required = true,dataType = "SysReport")
    public Results saveReport(@RequestBody SysReport reportDto) {
        log.warn(String.format("用户 %s 完成当日数据采集。", SecurityUtil.getLoginUser().getUsername()));
        return reportService.save(reportDto);
    }

    @GetMapping(value = "/data")
    public String data(Model model) {
        log.warn(String.format("用户 %s 进入数据监控页面。", SecurityUtil.getLoginUser().getUsername()));

        model.addAttribute("temperatureLine", reportService.countTemperatureLineData());
        model.addAttribute("temperature", reportService.countTemperatureData());
        model.addAttribute("travel", reportService.countTravelData());
        model.addAttribute("physicalCondition", reportService.countPhysicalConditionData());
        return "home/data";
    }

    @GetMapping(value = "chart/temperature")
    @ResponseBody
    public Results chartTemperature(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return reportService.getChartTemperatureData(startDateStr, endDateStr);
    }

    @GetMapping(value = "chart/travel")
    @ResponseBody
    public Results chartTravel(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return reportService.getChartTravelData(startDateStr, endDateStr);
    }

    @GetMapping(value = "chart/physicalCondition")
    @ResponseBody
    public Results chartPhysicalCondition(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return reportService.getChartPhysicalConditionData(startDateStr, endDateStr);
    }

    @GetMapping(value = "chart/temperatureLine")
    @ResponseBody
    public Results temperatureLine(Date startDate, Date endDate, Integer past) {
//        startDate = startDate == null ? new Date() : startDate;
        log.warn(String.format("用户 %s 进入数据监控页面。", SecurityUtil.getLoginUser().getUsername()));
        endDate = endDate == null ? new Date() : endDate;
        past = past == null ? 7 : past;
        if (startDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
            startDate = calendar.getTime();
        }
        return reportService.getChartTemperatureLine(startDate, endDate, past);
    }

    /*@GetMapping(value = "chart/count")
    @ResponseBody
    public Results chartCount(Date startDate, Date endDate, Integer past) {
//        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? new Date() : endDate;
        past = past == null ? 15 : past;
        if (startDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
            startDate = calendar.getTime();
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return reportService.getChartCountData(startDateStr, endDateStr);
    }*/

    @GetMapping(value = "chart/refresh")
    @ResponseBody
    public Results refresh() {
        return reportService.refresh();
    }



    String pattern = "yyyy-MM-dd";
    //只需要加上下面这段即可，注意不能忘记注解
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));// CustomDateEditor为自定义日期编辑器
    }

}
