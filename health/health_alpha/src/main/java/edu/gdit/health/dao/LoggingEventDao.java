package edu.gdit.health.dao;


import edu.gdit.health.model.LoggingEvent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 日志事件持久层
 */
@Mapper
public interface LoggingEventDao {

    /**
     * 获取日志列表
     * @param startPosition 分页起始
     * @param limit  分页间隔
     * @return Results 返回分页结果集对象
     */
    @Select("select * from logging_event t  ORDER BY timestmp desc limit #{startPosition}, #{limit} ")
    List<LoggingEvent> getListByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    /**
     * 统计日志总记录数
     * @return Long 总记录数
     */
    @Select("select count(*) from logging_event t")
    Long countAll();

}
