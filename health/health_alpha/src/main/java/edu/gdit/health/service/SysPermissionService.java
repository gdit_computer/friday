package edu.gdit.health.service;

import com.alibaba.fastjson.JSONArray;
import edu.gdit.health.base.result.Results;
import edu.gdit.health.model.SysPermission;

import java.util.List;

public interface SysPermissionService {

    /**
     * 获取所有菜单的权限值
     * @return Results 所有菜单的权限值
     */
    Results<JSONArray> listAllPermission();

    /**
     * 根据角色Id去查询拥有的权限
     * @return Results 角色拥有的权限
     */
    Results<SysPermission> listByRoleId(Long roleId);

    /**
     * 获取所有菜单的权限值
     * @return Results 所有菜单的权限值
     */
    Results<SysPermission> getMenuAll();


    /**
     * 保存用户新增的菜单信息
     * @return Results 所有菜单的权限值
     */
    Results<SysPermission> save(SysPermission sysPermission);

    /**
     * 获取用户所属角色下能显示的菜单
     * @param id
     * @return SysPermission
     */
    SysPermission getSysPermissionById(Long id);

    /**
     * 更新用户新增的菜单信息
     * @param sysPermission
     * @return Results
     */
    Results updateSysPermission(SysPermission sysPermission);

    /**
     * 删除用户新增的菜单信息
     * @param id
     * @return Results
     */
    Results delete(Long id);

    /**
     * 获取用户新增的菜单信息集合
     * @return List<SysPermission>
     */
    List<SysPermission> getMenu();

    /**
     * 根据用户ID获取用户新增的菜单信息集合
     * @return Results<SysPermission>
     */
    Results<SysPermission> getMenu(Long userId);
}
