package edu.gdit.health.controller;


import edu.gdit.health.base.result.Results;

import edu.gdit.health.service.SysRoleUserService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户角色关联控制器
 */
@RestController
@RequestMapping("/roleuser")
@Slf4j
public class SysRoleUserController {
    @Autowired
    private SysRoleUserService sysRoleUserService;

    /**
     * 获取当前用户角色
     * @param userId
     * @return
     */
    @PostMapping("/getRoleUserByUserId")
    @ApiOperation(value = "获取当前用户角色", notes = "获取当前用户角色")//描述
    @ApiImplicitParam(name = "userId",value = "用户Id", required = true)
    public Results getRoleUserByUserId(Long userId) {
        log.info("getRoleUserByUserId（"+userId+"）");
        return sysRoleUserService.getSysRoleUserByUserId(userId);
    }
}
