package edu.gdit.health.dao;


import edu.gdit.health.model.SysRoleUser;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 用户角色持久层
 */
@Mapper
public interface SysRoleUserDao {
    /**
     * 新增用户角色关联关系
     * @param sysRoleUser
     * @return
     */
    @Insert("insert into sys_role_user(userId, roleId) values(#{userId}, #{roleId})")
    int save(SysRoleUser sysRoleUser);

    /**
     * 根据用户id查找所有关联的数据
     * @param userId
     * @return  List<SysRoleUser>
     */
    @Select("select * from sys_role_user t where t.userId = #{userId}")
    SysRoleUser getSysRoleUserByUserId(Long userId);

    /**
     * 更新用户角色关联关系
     * @param sysRoleUser
     * @return int
     */
    int updateSysRoleUser(SysRoleUser sysRoleUser);

    /**
     * 删除用户角色关联关系
     * @param userId
     * @return int
     */
    @Delete("delete from sys_role_user where userId = #{userId}")
    int deleteRoleUserByUserId(Long userId);

    /**
     * 根据角色id查找所有关联的数据
     * @param roleId
     * @return List<SysRoleUser>
     */
    @Select("select * from sys_role_user t where t.roleId = #{roleId}")
    List<SysRoleUser> listAllSysRoleUserByRoleId(Long roleId);
}
