package edu.gdit.health.service.impl;

import com.alibaba.fastjson.JSONArray;


import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.SysPermissionDao;
import edu.gdit.health.model.SysPermission;
import edu.gdit.health.service.SysPermissionService;
import edu.gdit.health.utils.TreeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@Slf4j
public class SysPermissionServiceImpl implements SysPermissionService {

    @Autowired
    private SysPermissionDao sysPermissionDao;


    @Override
    public Results<JSONArray> listAllPermission() {
        log.info(getClass().getName() + ".listAllPermission()");
        List datas = sysPermissionDao.findAll();
        JSONArray array = new JSONArray();
        log.info(getClass().getName() + ".setPermissionsTree(?,?,?)");
        TreeUtils.setPermissionsTree(0L, datas, array);
        return Results.success(array);
    }

    @Override
    public Results<SysPermission> listByRoleId(Long roleId) {
        return Results.success(0, sysPermissionDao.listByRoleId(roleId));
    }

    @Override
    public Results<SysPermission> getMenuAll() {
        return Results.success(0, sysPermissionDao.findAll());
    }

    @Override
    public Results save(SysPermission sysPermission) {
        return (sysPermissionDao.save(sysPermission) > 0) ? Results.success() : Results.failure();
    }

    @Override
    public SysPermission getSysPermissionById(Long id) {
        return sysPermissionDao.getSysPermissionById(id);
    }

    @Override
    public Results updateSysPermission(SysPermission sysPermission) {
        return (sysPermissionDao.update(sysPermission) > 0) ? Results.success() : Results.failure();
    }

    @Override
    public Results delete(Long id) {
        sysPermissionDao.deleteById(id);
        sysPermissionDao.deleteByParentId(id);
        return Results.success();
    }

    @Override
    public List<SysPermission> getMenu() {
        return sysPermissionDao.findAll();
    }

    public Results getMenu(Long userId) {
        List<SysPermission> datas = sysPermissionDao.listByUserId(userId);
        datas = datas.stream().filter(p -> p.getType()==1).collect(Collectors.toList());
        JSONArray array = new JSONArray();
        log.info(getClass().getName() + ".setPermissionsTree(?,?,?)");
        TreeUtils.setPermissionsTree(0L, datas, array);
        return Results.success(array);
    }

}
