package com.sxbang.friday.controller.api;

import com.sxbang.friday.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
@RequestMapping("${api-url}")
public class ApiController {

    @RequestMapping(value="/getPage")
    public ModelAndView getPage(ModelAndView modelAndView, String pageName){
        log.warn(String.format("用户 %s 打开了页面[%s]。", SecurityUtil.getLoginUser().getUsername(), pageName));
        modelAndView.setViewName(pageName);
        return modelAndView;
    }

}
