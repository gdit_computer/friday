package edu.gdit.health.service;


import edu.gdit.health.base.result.Results;

public interface SysRoleUserService {

    /**
     * 根据用户id获取用户角色
     * @param userId
     * @return Results
     */
    Results getSysRoleUserByUserId(Long userId);

}
