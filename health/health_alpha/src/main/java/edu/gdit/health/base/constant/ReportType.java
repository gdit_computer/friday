package edu.gdit.health.base.constant;

/**
 * 数据填报记录类型枚举
 */
public enum ReportType {
    temperature,
    travel,
    physical_condition,
}
