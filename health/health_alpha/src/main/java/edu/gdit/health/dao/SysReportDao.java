package edu.gdit.health.dao;

import edu.gdit.health.model.SysReport;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * 数据采集持久层
 */
@Mapper
public interface SysReportDao {

    /**
     * 查询所有数据采集信息
     * @return List<SysReport>
     */
    @Select("select * from sys_report t ")
    List<SysReport> getAll ();

    /**
     * 按id查询数据采集信息
     * @param id
     * @return SysReport
     */
    @Select("select * from sys_report t where t.id=#{id} ")
    SysReport getById (Integer id);

    /**
     * 增加数据采集信息
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_report(user_id, temperature, travel, remark, travel_description, physical_condition, createTime) " +
            "values(#{userId}, #{temperature}, #{travel}, #{remark}, #{travelDescription}, #{physicalCondition}, now())")
    int save(SysReport report);

    /**
     * 修改数据采集信息
     * @param report
     * @return
     */
    @Update(" UPDATE `sys_report` SET " +
            " `user_id`=#{userId}, `createTime`=#{createTime}, " +
            "`remark`=#{remark}, `temperature`=#{temperature}, `travel`=#{travel}, " +
            "`travel_description`=#{travelDescription}, `physical_condition`=#{physicalCondition} " +
            "WHERE (`id`=#{id}) LIMIT 1 ")
    int update(SysReport report);

    /**
     * 根据id删除数据采集信息
     * @param id
     */
    @Delete("delete from sys_report where id = #{id}")
    int delete(Integer id);

    /**
     * 近7天体温趋势/近7天体温趋势信息
     * @return
     */
    @Select("SELECT COUNT(*) as `value`,t.temperature as `name` from  sys_report t WHERE DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} GROUP BY t.temperature")
    List<Map<String, Object>> getChartTemperatureData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);



    /**
     * 当日旅行数据信息
     * @return
     */
    @Select("SELECT COUNT(*) as `value`,concat(t.travel,'')  as `name` from  sys_report t WHERE DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} GROUP BY t.travel")
    List<Map<String, Object>> getChartTravelData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);

    /**
     * 统计当前用户的统采集次数
     * @return
     */
    @Select("select count(*) from sys_report t where t.user_id = #{userId}")
    Long countByUserId(@Param("userId")Long userId);

    /**
     * 统计当前用户的统采集次数
     * @return
     */

    @Select("select * from sys_report t where t.user_id = #{userId}  ORDER BY t.createTime desc limit #{startPosition},#{limit}")
    List<SysReport> getReportsByIdAndPage(@Param("userId")Long userId, @Param("startPosition")Integer startPosition,@Param("limit")Integer limit);


    /**
     * 当日症状数据信息
     * @return
     */
    @Select(" SELECT " +
            " SUM(FIND_IN_SET('travel', t.physical_condition) >0) as travel " +
            " ,SUM(FIND_IN_SET('normal', t.physical_condition) >0) as normal " +
            " ,SUM(FIND_IN_SET('contact', t.physical_condition) >0) as contact " +
            " ,SUM(FIND_IN_SET('rheum', t.physical_condition) >0) as rheum " +
            " ,SUM(FIND_IN_SET('polypnea', t.physical_condition) >0) as polypnea " +
            " ,SUM(FIND_IN_SET('vomiting', t.physical_condition) >0) as vomiting " +
            " ,SUM(FIND_IN_SET('flustered', t.physical_condition) >0) as flustered " +
            " ,SUM(FIND_IN_SET('conjunctivitis', t.physical_condition) >0) as conjunctivitis " +
            " FROM sys_report t where " +
            " DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  " +
            " and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} " )
    Map<String, Object> getChartPhysicalConditionData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);

    /**
     * 统计所有的统采集数
     * @return
     */
    @Select(" SELECT  " +
            " count(*) as `value` , DATE_FORMAT(t.createTime, '%Y-%m-%d')  as `name`  " +
            " FROM sys_report t where  " +
            " DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  " +
            " and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr}  " +
            " GROUP BY DATE_FORMAT(t.createTime, '%Y-%m-%d')  " )
    List<Map<String, Object>> getChartCountData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);
}
