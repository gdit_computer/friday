package edu.gdit.health.dao;

import edu.gdit.health.model.SysPermission;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 权限持久层
 */
@Mapper
public interface SysPermissionDao {

    /**
     * 获取所有菜单的权限值
     * @return  List<SysPermission> 所有菜单的权限值
     */
    @Select("select * from sys_permission t")
    List<SysPermission> findAll();

    /**
     * 根据角色Id去查询拥有的权限
     * @return List<SysPermission>角色拥有的权限
     */
    @Select("select p.* from sys_permission p inner join sys_role_permission rp on p.id = rp.permissionId where rp.roleId = #{roleId} order by p.sort")
    List<SysPermission> listByRoleId(Long roleId);

    /**
     * 保存用户新增的菜单信息
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_permission(parentId, name, css, href, type, permission, sort) values(#{parentId}, #{name}, #{css}, #{href}, #{type}, #{permission}, #{sort})")
    int save(SysPermission e);

    @Select("select * from sys_permission t where t.id = #{id}")
    SysPermission getSysPermissionById(Long id);

    /**
     * 更新用户新增的菜单信息
     */
    int update(SysPermission e);


    /**
     * 根据id删除用户新增的菜单信息
     * @param id
     * @return
     */
    @Delete("delete from sys_permission where id = #{id}")
    int deleteById(Long id);

    /**
     * 根据父节点id删除用户新增的菜单信息
     * @param parentId
     * @return
     */
    @Delete("delete from sys_permission where parentId = #{parentId}")
    int deleteByParentId(Long parentId);

    /**
     * 根据用户ID获取用户新增的菜单信息集合
     * @return List<SysPermission>
     */
    @Select("SELECT DISTINCT sp.*  " +
            "FROM sys_role_user sru " +
            "INNER JOIN sys_role_permission srp ON srp.roleId = sru.roleId " +
            "LEFT JOIN sys_permission sp ON srp.permissionId = sp.id " +
            "WHERE " +
            "sru.userId = #{userId}")
    List<SysPermission> listByUserId(@Param("userId") Long userId);
}
