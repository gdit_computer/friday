package edu.gdit.health.utils;

import edu.gdit.health.model.bean.LoginUser;
import org.springframework.security.core.context.SecurityContextHolder;

public class SecurityUtil {

    /**
     * 获取当前用户
     *
     * @return 取不到返回 new User()
     */
    public static LoginUser getLoginUser() {
        try {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal != null) {
                LoginUser user =(LoginUser)principal;
                if (user != null) {
                    return user;
                }
                return new LoginUser();
            }
        } catch (Exception ex) {
        }
        // 如果没有登录，则返回实例化空的User对象。
        return new LoginUser();
    }
}
