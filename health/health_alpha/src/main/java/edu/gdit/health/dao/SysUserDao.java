package edu.gdit.health.dao;

import edu.gdit.health.model.SysUser;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 用户信息持久层
 */
@Mapper
public interface SysUserDao {

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return SysUser
     */
    @Select("select * from sys_user t where t.username = #{username}")
    SysUser getUser(String username);

    /**
     * 童虎用户总数
     * @return Long
     */
    @Select("select count(*) from sys_user t ")
    Long countAllUsers();

    /**
     * 获取用户信息列表分页
     * @param startPosition
     * @param limit
     * @return List<SysUser>
     */
    @Select("select * from sys_user t order by t.id limit #{startPosition},#{limit}")
    List<SysUser> getAllUsersByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    /**
     * 根据手机号获取用户信息
     * @param telephone
     * @return SysUser
     */
    @Select("select * from sys_user t where t.telephone = #{telephone}")
    SysUser getUserByPhone(String telephone);

    /**
     * 根据电子邮箱获取用户信息
     * @param email
     * @return SysUser
     */
    @Select("select * from sys_user t where t.email = #{email}")
    SysUser getUserByEmail(String email);

    /**
     * 保存用户信息
     * @param user
     * @return int
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_user(username, password, nickname, headImgUrl, phone, telephone, email, birthday, sex, status, createTime, updateTime) values(#{username}, #{password}, #{nickname}, #{headImgUrl}, #{phone}, #{telephone}, #{email}, #{birthday}, #{sex}, #{status}, now(), now())")
    int save(SysUser user);

    /**
     * 根据id获取用户信息
     * @param id
     * @return SysUser
     */
    @Select("select * from sys_user t where t.id = #{id}")
    SysUser getById(Long id);

    /**
     * 更新用户信息
     * @param user
     * @return int
     */
    int updateUser(SysUser user);

    /**
     * 删除用户信息
     * @param id
     * @return int
     */
    @Delete("delete from sys_user where id = #{id}")
    int deleteUser(Long id);

    /**
     * 统计模糊搜索查询用户信息数量
     * @param username
     * @return Long
     */
    @Select("select count(*) from sys_user t where t.username like '%${username}%'")
    Long getUserByFuzzyUserName(@Param("username") String username);

    /**
     * 模糊搜索查询用户信息并分页
     * @param username
     * @param startPosition
     * @param limit
     * @return List<SysUser>
     */
    @Select("select * from sys_user t where t.username like '%${username}%' limit #{startPosition},#{limit}")
    List<SysUser> getUserByFuzzyUserNamePage(@Param("username") String username,@Param("startPosition")Integer startPosition,@Param("limit")Integer limit);

    /**
     * 修改密码
     * @param id
     * @param password
     * @return int
     */
    @Update("update sys_user t set t.password = #{password} where t.id = #{id}")
    int changePassword(@Param("id") Long id, @Param("password") String password);
}
