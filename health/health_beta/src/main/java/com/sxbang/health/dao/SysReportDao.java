package com.sxbang.health.dao;

import com.sxbang.health.model.SysReport;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface SysReportDao {

    @Select("select * from sys_report t ")
    List<SysReport> getAll ();

    @Select("select * from sys_report t where t.id=#{id} ")
    SysReport getById (Integer id);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_report(user_id, temperature, travel, remark, travel_description, physical_condition, createTime) " +
            "values(#{userId}, #{temperature}, #{travel}, #{remark}, #{travelDescription}, #{physicalCondition}, now())")
    int save(SysReport report);

    @Update(" UPDATE `sys_report` SET " +
            " `user_id`=#{userId}, `createTime`=#{createTime}, " +
            "`remark`=#{remark}, `temperature`=#{temperature}, `travel`=#{travel}, " +
            "`travel_description`=#{travelDescription}, `physical_condition`=#{physicalCondition} " +
            "WHERE (`id`=#{id}) LIMIT 1 ")
    int update(SysReport report);

    @Delete("delete from sys_report where id = #{id}")
    int delete(Integer id);

    @Select("SELECT COUNT(*) as `value`,t.temperature as `name` from  sys_report t WHERE DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} GROUP BY t.temperature")
    List<Map<String, Object>> getChartTemperatureData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);

    @Select("SELECT COUNT(*) as `value`,concat(t.travel,'')  as `name` from  sys_report t WHERE DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} GROUP BY t.travel")
    List<Map<String, Object>> getChartTravelData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);

    @Select(" SELECT " +
            " SUM(FIND_IN_SET('travel', t.physical_condition) >0) as travel " +
            " ,SUM(FIND_IN_SET('normal', t.physical_condition) >0) as normal " +
            " ,SUM(FIND_IN_SET('contact', t.physical_condition) >0) as contact " +
            " ,SUM(FIND_IN_SET('rheum', t.physical_condition) >0) as rheum " +
            " ,SUM(FIND_IN_SET('polypnea', t.physical_condition) >0) as polypnea " +
            " ,SUM(FIND_IN_SET('vomiting', t.physical_condition) >0) as vomiting " +
            " ,SUM(FIND_IN_SET('flustered', t.physical_condition) >0) as flustered " +
            " ,SUM(FIND_IN_SET('conjunctivitis', t.physical_condition) >0) as conjunctivitis " +
            " FROM sys_report t where " +
            " DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  " +
            " and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} " )
    Map<String, Object> getChartPhysicalConditionData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);

    @Select(" SELECT  " +
            " count(*) as `value` , DATE_FORMAT(t.createTime, '%Y-%m-%d')  as `name`  " +
            " FROM sys_report t where  " +
            " DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  " +
            " and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr}  " +
            " GROUP BY DATE_FORMAT(t.createTime, '%Y-%m-%d')  " )
    List<Map<String, Object>> getChartCountData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);
}
