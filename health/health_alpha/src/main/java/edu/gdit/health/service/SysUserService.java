package edu.gdit.health.service;


import edu.gdit.health.base.result.Results;
import edu.gdit.health.dto.SysUserDto;
import edu.gdit.health.model.SysUser;

public interface SysUserService {

    /**
     * 根据用户名获取用户信息
     * @param username
     * @return SysUser
     */
    SysUser getUser(String username);

    /**
     * 获取用户信息列表分页
     * @param startPosition
     * @param limit
     * @return  Results<SysUser>
     */
    Results<SysUser> getAllUsersByPage(Integer startPosition, Integer limit);


    /**
     * 根据手机号获取用户信息
     * @param phone
     * @return SysUser
     */
    SysUser getUserByPhone(String phone);
    /**
     * 根据电子邮箱获取用户信息
     * @param email
     * @return SysUser
     */
    SysUser getUserByEmail(String email);

    /**
     * 保存编辑完的用户信息
     * @param user
     * @param roleId
     * @return Results
     */
    Results save(SysUser user, Long roleId);
    /**
     * 根据id获取用户信息
     * @param id
     * @return SysUser
     */
    SysUser getUserById(Long id);

    /**
     * 修改用户信息
     * @param sysUserDto
     * @param roleId
     * @return
     */
    Results updateUser(SysUserDto sysUserDto, Long roleId);

    /**
     * 删除用户信息
     * @param id
     * @return Results
     */
    int deleteUser(Long id);

    /**
     * 模糊搜索查询用户信息并分页
     * @param username
     * @param startPosition
     * @param limit
     * @return Results<SysUser>
     */
    Results<SysUser> getUserByFuzzyUserNamePage(String username, Integer startPosition, Integer limit);

    /**
     * 修改密码
     * @param username
     * @param oldPassword
     * @param newPassword
     * @return Results
     */
    Results changePassword(String username, String oldPassword, String newPassword);

}
