package edu.gdit.health.dao;

import edu.gdit.health.model.SysReportCount;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 数据采集信息统计持久层
 */
@Mapper
public interface SysReportCountDao {


    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_report_count(`name`, `value`, `type`, report_date, createTime) " +
            "values(#{name}, #{value}, #{type}, #{reportDate}, #{createTime})")
    int save(SysReportCount reportCount);

    @Select("select * from sys_report_count t where `type`=#{type} and DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDate}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDate} ")
    List<SysReportCount> query(@Param("type")String type, @Param("startDate")String startDate, @Param("endDate")String endDate);

    @Delete("delete from sys_report_count where report_date = #{reportDate} and `type`=#{type} and `name` = #{name} ")
    void delete(SysReportCount item);

    @Select("select ifnull(SUM(t.`value`), 0) from sys_report_count t where `type` = #{type} and `report_date` = #{startDate} and t.`name` in (${keys})")
    long sumValueByType(String type, String startDate, String keys);
}
