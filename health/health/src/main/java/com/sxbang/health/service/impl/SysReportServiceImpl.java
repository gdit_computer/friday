package com.sxbang.health.service.impl;

import com.sxbang.health.base.result.Results;
import com.sxbang.health.dao.SysReportDao;
import com.sxbang.health.model.SysDict;
import com.sxbang.health.model.SysReport;
import com.sxbang.health.service.SysReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Service
public class SysReportServiceImpl implements SysReportService {

    @Resource
    SysReportDao sysReportDao;


    @Override
    public Map<String, ArrayList> getDictSet() {
        Map<String, ArrayList> rs = new HashMap<>();
        rs.put("temperature", new ArrayList<SysDict>());
        rs.put("travel", new ArrayList<SysDict>());
        rs.put("physical_condition", new ArrayList<SysDict>());
        rs.get("temperature").add(new SysDict("NORMAL" , "正常 37.3℃ 以下"));
        rs.get("temperature").add(new SysDict("LOW" , "低热 37.3-37.9℃"));
        rs.get("temperature").add(new SysDict("MIDDLE" , "中热 38-39℃"));
        rs.get("temperature").add(new SysDict("HIGH" , "高热 39℃以上"));
        rs.get("travel").add(new SysDict("1" , "有旅游"));
        rs.get("travel").add(new SysDict("0" , "没有旅游"));
        rs.get("physical_condition").add(new SysDict("travel" , "14天内曾居住或前往疫情高发地"));
        rs.get("physical_condition").add(new SysDict("contact" , "两周内有与确诊患者接触"));
        rs.get("physical_condition").add(new SysDict("normal" , "没有出现症状"));
        rs.get("physical_condition").add(new SysDict("rheum" , "感冒样症状：乏力、精神差、咳嗽、发烧、肌肉痛、头痛"));
        rs.get("physical_condition").add(new SysDict("polypnea" , "喘憋、呼吸急促"));
        rs.get("physical_condition").add(new SysDict("vomiting" , "恶心呕吐、腹泻"));
        rs.get("physical_condition").add(new SysDict("flustered" , "心慌、胸闷"));
        rs.get("physical_condition").add(new SysDict("conjunctivitis" , "结膜炎（红眼病样表现：眼睛涩、红、分泌物）"));
        return rs;
    }

    @Override
    public Results save(SysReport report) {
        report.setUserId(1L);
        return Results.success(sysReportDao.save(report));
    }
}
