package com.sxbang.friday.dao;

import com.sxbang.friday.model.SysReport;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReportDao {

    @Select("select * from sys_report t")
    List<SysReport> getAllReports();

    @Select("select count(*) from sys_report t")
    Long countAllReports();

    @Select("select count(*) from sys_report t where t.user_id = #{userId}")
    Long countByUserId(@Param("userId")Long userId);

    @Select("select * from sys_report t limit #{startPosition}, #{limit}")
    List<SysReport> getAllReportsByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_report(user_id, temperature, travel, remark, travel_description, physical_condition, createTime) " +
            "values(#{userId}, #{temperature}, #{travel}, #{remark}, #{travelDescription}, #{physicalCondition}, now())")
    int save(SysReport report);

//    int saveReport(SysReport report);

    @Select("select * from sys_report t where t.id = #{id}")
    SysReport getById(Integer id);

//    int update(SysReport report);

    @Delete("delete from sys_report where id = #{id}")
    int delete(Integer id);

    @Select("select count(*) from sys_report t where t.name like '%${reportName}%'")
    Long countReportByFuzzyReportName(@Param("reportName") String reportName);

    @Select("select * from sys_report t where t.name like '%${reportName}%' limit #{startPosition},#{limit}")
    List<SysReport> getReportByFuzzyReportNamePage(@Param("reportName") String reportName,@Param("startPosition")Integer startPosition,@Param("limit")Integer limit);

    @Select("select * from sys_report t where t.user_id = #{userId}  ORDER BY t.createTime desc limit #{startPosition},#{limit}")
    List<SysReport> getReportsByIdAndPage(@Param("userId")Long userId, @Param("startPosition")Integer startPosition,@Param("limit")Integer limit);

    /*@Select(" SELECT " +
            " count(t.id) as `value`,d.dict_key  as `name` " +
            " FROM  sys_dict d " +
            " LEFT JOIN  sys_report t ON t.temperature = d.dict_key AND " +
            " ( DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr} " +
            " and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} ) " +
            " WHERE d.dict_type = 'temperature' " +
            " GROUP BY d.dict_key " )*/
    @Select("SELECT COUNT(*) as `value`,t.temperature  as `name` from  sys_report t WHERE DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} GROUP BY t.temperature")
    List<Map<String, Object>> getChartTemperatureData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);

    @Select("SELECT COUNT(*) as `value`,concat(t.travel,'')  as `name` from  sys_report t WHERE DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} GROUP BY t.travel")
    List<Map<String, Object>> getChartTravelData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);

    @Select(" SELECT " +
            " SUM(FIND_IN_SET('travel', t.physical_condition) >0) as travel " +
            " ,SUM(FIND_IN_SET('normal', t.physical_condition) >0) as normal " +
            " ,SUM(FIND_IN_SET('contact', t.physical_condition) >0) as contact " +
            " ,SUM(FIND_IN_SET('rheum', t.physical_condition) >0) as rheum " +
            " ,SUM(FIND_IN_SET('polypnea', t.physical_condition) >0) as polypnea " +
            " ,SUM(FIND_IN_SET('vomiting', t.physical_condition) >0) as vomiting " +
            " ,SUM(FIND_IN_SET('flustered', t.physical_condition) >0) as flustered " +
            " ,SUM(FIND_IN_SET('conjunctivitis', t.physical_condition) >0) as conjunctivitis " +
            " FROM sys_report t where " +
            " DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  " +
            " and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr} " )
    Map<String, Object> getChartPhysicalConditionData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);



    /*@Select(" SELECT  " +
            " count(*) as `value` , DATE_FORMAT(t.createTime, '%Y-%m-%d')  as `name`  " +
            " FROM sys_report t where  " +
            " DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDateStr}  " +
            " and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDateStr}  " +
            " GROUP BY DATE_FORMAT(t.createTime, '%Y-%m-%d')  " )
    List<Map<String, Object>> getChartCountData(@Param("startDateStr")String startDateStr, @Param("endDateStr")String endDateStr);
*/


}
