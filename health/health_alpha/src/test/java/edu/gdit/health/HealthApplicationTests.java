package edu.gdit.health;

import edu.gdit.health.dao.SysReportDao;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
class HealthApplicationTests {

    @Resource
    SysReportDao sysReportDao;

    @Test
    void contextLoads() {
        System.out.println("contextLoads ... ");
    }

    @Test
    public void testReportGetAll() {
        System.out.println("testReportGetAll ... ");
        sysReportDao.getAll().stream().forEach(System.out::println);
    }
}
