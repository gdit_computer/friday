package com.sxbang.health.service.impl;

import com.sxbang.health.dao.SysUserDao;
import com.sxbang.health.model.SysUser;
import com.sxbang.health.service.SysUserService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserDao sysUserDao;


    @Override
    public SysUser getUser(String username) {
        return sysUserDao.getUser(username);
    }
}
