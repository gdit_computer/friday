package com.sxbang.friday.service;

import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.model.SysReport;

import java.util.Date;

public interface ReportService {

	Results<SysReport> getAllReports();

	Results<SysReport> getAllReportsByPage(Integer offset, Integer limit);

	Results<SysReport> save(SysReport roleDto);

	SysReport getReportById(Integer id);

//	Results update(SysReport roleDto);

	Results delete(Integer id);

	Results<SysReport> getReportByFuzzyReportNamePage(String roleName, Integer offset, Integer limit);

    Results getSelfReportsByPage(Integer offset, Integer limit);


	Results getChartTemperatureData(String startDateStr, String endDateStr);

	Results getChartTravelData(String startDateStr, String endDateStr);

	Results getChartPhysicalConditionData(String startDateStr, String endDateStr);

//	Results getChartCountData(String startDateStr, String endDateStr);

	void countReportData(Date now);

    Results refresh();

	Results getChartTemperatureLine(Date startDateStr, Date endDateStr, int past);

    long countTemperatureLineData();

	long countTemperatureData();

	long countTravelData();

	long countPhysicalConditionData();

}
