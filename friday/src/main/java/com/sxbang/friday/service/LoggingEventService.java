package com.sxbang.friday.service;

import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.model.LoggingEvent;
import com.sxbang.friday.model.SysDict;

import java.util.ArrayList;
import java.util.Map;

public interface LoggingEventService {

	Results getAll();

	Results getAllByPage(Integer offset, Integer limit);

	LoggingEvent getById(Integer id);

}
