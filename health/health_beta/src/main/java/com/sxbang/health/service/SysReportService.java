package com.sxbang.health.service;

import com.sxbang.health.base.result.Results;
import com.sxbang.health.model.SysReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public interface SysReportService {

    Map<String, ArrayList> getDictSet();

    Results save(SysReport report);

    void countReportData(Date now);

    Results refresh();

    Results getChartTemperatureLine(Date startDate, Date endDate, Integer past);

    Results getChartTemperatureData(String startDateStr, String endDateStr);

    Results getChartTravelData(String startDateStr, String endDateStr);

    Results getChartPhysicalConditionData(String startDateStr, String endDateStr);

    long countTemperatureLineData();

    long countTemperatureData();

    long countTravelData();

    long countPhysicalConditionData();
}
