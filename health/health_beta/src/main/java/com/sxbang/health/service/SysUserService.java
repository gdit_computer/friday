package com.sxbang.health.service;

import com.sxbang.health.base.result.Results;
import com.sxbang.health.model.SysUser;

public interface SysUserService {

    SysUser getUser(String username);
}
