package edu.gdit.health.service;


import edu.gdit.health.base.result.Results;
import edu.gdit.health.dto.SysRoleDto;
import edu.gdit.health.model.SysRole;

public interface SysRoleService {

	/**
	 * 获取所有角色信息
	 * @return Results<SysRole>
	 */
	Results<SysRole> getAllRoles();

	/**
	 * 查询用户角色列表（分页）
	 * @param offset
	 * @param limit
	 * @return Results<SysRole>
	 */
	Results<SysRole> getAllRolesByPage(Integer offset, Integer limit);

	/**
	 * 保存角色信息
	 * @param roleDto
	 * @return Results<SysRole>
	 */
	Results<SysRole> save(SysRoleDto roleDto);

	/**
	 * 根据id查询角色信息
	 * @param id
	 * @return SysRole
	 */
	SysRole getRoleById(Long id);

	/**
	 * 更新角色信息
	 * @param roleDto
	 * @return Results
	 */
	Results update(SysRoleDto roleDto);

	/**
	 * 删除角色信息
	 * @param id
	 * @return Results
	 */
	Results delete(Long id);

	/**
	 * 模糊搜索查询角色信息并分页
	 * @param roleName
	 * @param offset
	 * @param limit
	 * @return Results<SysRole>
	 */
	Results<SysRole> getRoleByFuzzyRoleNamePage(String roleName, Integer offset, Integer limit);
}
