package com.sxbang.health.dao;

import com.sxbang.health.model.LoggingEvent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface LoggingEventDao {

    @Select("select * from logging_event t  ORDER BY timestmp desc limit #{startPosition}, #{limit} ")
    List<LoggingEvent> getListByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    @Select("select count(*) from logging_event t")
    Long countAll();

}
