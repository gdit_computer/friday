package com.sxbang.friday.service;

import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.dto.RoleDto;
import com.sxbang.friday.model.SysDict;
import com.sxbang.friday.model.SysRole;

import java.util.ArrayList;
import java.util.Map;

public interface DictService {

	Results<SysDict> getAllDicts();

	Results<SysDict> getAllDictsByPage(Integer offset, Integer limit);

	Results<SysDict> save(SysDict roleDto);

	SysDict getDictById(Integer id);

	Results update(SysDict roleDto);

	Results delete(Integer id);

	Results<SysDict> getDictByFuzzyDictNamePage(String roleName, Integer offset, Integer limit);


	Results getDictByTypes(String[] types);

	Results getDictByType(String dictType);

	Map<String, ArrayList> getDictSet(String[] types);
}
