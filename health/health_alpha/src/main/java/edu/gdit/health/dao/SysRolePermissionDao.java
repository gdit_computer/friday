package edu.gdit.health.dao;


import edu.gdit.health.model.SysRolePermission;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户权限持久层
 */
@Mapper
public interface SysRolePermissionDao {

    /**
     * 删除角色权限
     * @param rolePermission
     * @return int
     */
    @Delete("delete from sys_role_permission where permissionId = #{permissionId}")
    int delete(SysRolePermission rolePermission);

    /**
     * 保存用户与角色关联关系
     * @param id
     * @param permissionIds
     * @return
     */
    int save(@Param("roleId")Long id, @Param("permissionIds") List<Long> permissionIds);

    /**
     * 删除角色权限关联关系
     * @param roleId
     * @return
     */
    @Delete("delete from sys_role_permission where roleId = #{roleId}")
    int deleteRolePermission(Long roleId);
}
