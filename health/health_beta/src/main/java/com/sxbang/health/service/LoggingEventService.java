package com.sxbang.health.service;

import com.sxbang.health.base.result.Results;

public interface LoggingEventService {

    Results getListByPage(Integer offset, Integer limit);
}
