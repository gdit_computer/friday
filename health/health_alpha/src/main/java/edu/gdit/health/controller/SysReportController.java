package edu.gdit.health.controller;

import edu.gdit.health.base.result.PageTableRequest;
import edu.gdit.health.base.result.Results;
import edu.gdit.health.model.SysReport;
import edu.gdit.health.service.SysReportService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

/**
 * 数据监控控制器
 */
@Controller
@RequestMapping("/report")
public class SysReportController {

    private static final Log log = LogFactory.getLog(SysReportController.class);

    @Resource
    SysReportService sysReportService;

    /**
     * 用户分页获取数据采集信息
     * @param request
     * @return Results
     */
    @GetMapping("self/list")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:report:add')")
    @ApiOperation(value = "分页获取采集信息", notes = "用户分页获取数据采集信息")//描述
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "limit", required = true,dataType = "Integer"),
    })
    public Results selfList(PageTableRequest request) {
        log.info("ReportController.selfList(): param ( request = " + request +" )");
        request.countOffset();
//		return reportService.getAllReportsByPage(request.getOffset(), request.getLimit());
        return sysReportService.getSelfReportsByPage(request.getOffset(), request.getLimit());
    }

    /**
     * 跳转到增加数据采集页面
     * @param model
     * @return String
     */
    @GetMapping(value = "add")
    public String addReport(Model model) {
        log.warn("用户admin 进入数据采集页面。 ");
        model.addAttribute("sysReport",new SysReport());
        Map<String, ArrayList> dict = sysReportService.getDictSet();
        model.addAttribute("reportDict", dict);
        return "report/report";
    }

    /**
     * 增加数据采集信息
     * @param report
     * @return String
     */
    @PostMapping(value = "add")
    @ResponseBody
    public Results saveReport(@RequestBody SysReport report) {
        return sysReportService.save(report);
    }

    /**
     * 进入数据监控页面
     * @param model
     * @return String
     */
    @GetMapping(value = "/data")
    public String data(Model model) {
        log.warn("用户 %s 进入数据监控页面。");

        model.addAttribute("temperatureLine", sysReportService.countTemperatureLineData());
        model.addAttribute("temperature", sysReportService.countTemperatureData());
        model.addAttribute("travel", sysReportService.countTravelData());
        model.addAttribute("physicalCondition", sysReportService.countPhysicalConditionData());
        return "report/data";
    }

    /**
     * 刷新数据监控业务
     * @return Results
     */
    @GetMapping(value = "chart/refresh")
    @ResponseBody
    public Results refresh() {
        return sysReportService.refresh();
    }

    /**
     * 近7天体温趋势
     * @param startDate
     * @param endDate
     * @param past 体温趋势日期范围,为null时默认为7天
     * @return
     */
    @GetMapping(value = "chart/temperatureLine")
    @ResponseBody
    public Results temperatureLine(Date startDate, Date endDate, Integer past) {
        endDate = endDate == null ? new Date() : endDate;
        past = past == null ? 7 : past;
        if (startDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
            startDate = calendar.getTime();
        }
        return sysReportService.getChartTemperatureLine(startDate, endDate, past);
    }

    /**
     * 当日体温数据
     * @param startDate
     * @param endDate
     * @return
     */
    @GetMapping(value = "chart/temperature")
    @ResponseBody
    public Results chartTemperature(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return sysReportService.getChartTemperatureData(startDateStr, endDateStr);
    }

    /**
     * 当日旅行数据
     * @param startDate
     * @param endDate
     * @return
     */
    @GetMapping(value = "chart/travel")
    @ResponseBody
    public Results chartTravel(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return sysReportService.getChartTravelData(startDateStr, endDateStr);
    }

    /**
     * 当日症状数据
     * @param startDate
     * @param endDate
     * @return
     */
    @GetMapping(value = "chart/physicalCondition")
    @ResponseBody
    public Results chartPhysicalCondition(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return sysReportService.getChartPhysicalConditionData(startDateStr, endDateStr);
    }

    //Date日期格式
    String pattern = "yyyy-MM-dd";
    //为当前控制器注册一个Date日期类型属性编辑器
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));// CustomDateEditor为自定义日期编辑器
    }

}
