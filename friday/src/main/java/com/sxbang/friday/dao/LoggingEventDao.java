package com.sxbang.friday.dao;

import com.sxbang.friday.model.LoggingEvent;
import com.sxbang.friday.model.SysDict;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LoggingEventDao {

    @Select("select * from logging_event t")
    List<LoggingEvent> getAll();

    @Select("select * from logging_event t  ORDER BY timestmp desc limit #{startPosition}, #{limit} ")
    List<LoggingEvent> getAllByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    @Select("select count(*) from logging_event t")
    Long countAll();

    @Select("select * from logging_event t where t.event_id = #{id}")
    LoggingEvent getById(Integer id);



}
