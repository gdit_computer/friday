package edu.gdit.health.service.impl;


import edu.gdit.health.base.result.ResponseCode;
import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.SysRoleDao;
import edu.gdit.health.dao.SysRolePermissionDao;
import edu.gdit.health.dao.SysRoleUserDao;
import edu.gdit.health.dto.SysRoleDto;
import edu.gdit.health.model.SysRole;
import edu.gdit.health.model.SysRoleUser;
import edu.gdit.health.service.SysRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
@Slf4j
public class SysRoleServiceImpl implements SysRoleService {

	@Autowired
	private SysRoleDao sysRoleDao;

    @Autowired
    SysRolePermissionDao sysRolePermissionDao;

    @Autowired
    private SysRoleUserDao sysRoleUserDao;

	@Override
	public Results<SysRole> getAllRoles() {
		return Results.success(50, sysRoleDao.getAllRoles());
	}

	@Override
	public Results<SysRole> getAllRolesByPage(Integer offset, Integer limit) {
        return Results.success(sysRoleDao.countAllRoles().intValue(), sysRoleDao.getAllRolesByPage(offset, limit));
	}


    public Results<SysRole> save(SysRoleDto roleDto) {
        //1、先保存角色"
        sysRoleDao.save(roleDto);
//        roleDao.saveRole(roleDto);
        List<Long> permissionIds = roleDto.getPermissionIds();
        //移除0,permission id是从1开始
        permissionIds.remove(0L);
        //2、保存角色对应的所有权限
        if (!CollectionUtils.isEmpty(permissionIds)) {
            sysRolePermissionDao.save(roleDto.getId(), permissionIds);
        }
        return Results.success();
    }


    public SysRole getRoleById(Long id) {
        return sysRoleDao.getById(id);
    }


    public Results update(SysRoleDto roleDto) {
        List<Long> permissionIds = roleDto.getPermissionIds();
        permissionIds.remove(0L);
        //1、更新角色权限之前要删除该角色之前的所有权限
        sysRolePermissionDao.deleteRolePermission(roleDto.getId());

        //2、判断该角色是否有赋予权限值，有就添加"
        if (!CollectionUtils.isEmpty(permissionIds)) {
            sysRolePermissionDao.save(roleDto.getId(), permissionIds);
        }

        //3、更新角色表
        int countData = sysRoleDao.update(roleDto);

        if(countData > 0){
            return Results.success();
        }else{
            return Results.failure();
        }
    }


    public Results delete(Long id) {
        List<SysRoleUser> datas = sysRoleUserDao.listAllSysRoleUserByRoleId(id);
        if(datas.size() <= 0){
            sysRoleDao.delete(id);
            return Results.success();
        }
        return Results.failure(ResponseCode.USERNAME_REPEAT.USER_ROLE_NO_CLEAR.getCode(),ResponseCode.USERNAME_REPEAT.USER_ROLE_NO_CLEAR.getMessage());
    }

    @Override
    public Results<SysRole> getRoleByFuzzyRoleNamePage(String roleName, Integer startPosition, Integer limit) {
        return Results.success(sysRoleDao.countRoleByFuzzyRoleName(roleName).intValue(), sysRoleDao.getRoleByFuzzyRoleNamePage(roleName,startPosition, limit));
    }
}
