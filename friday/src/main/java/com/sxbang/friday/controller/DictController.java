package com.sxbang.friday.controller;

import com.sxbang.friday.base.result.PageTableRequest;
import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.model.SysDict;
import com.sxbang.friday.service.DictService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("dict")
@Slf4j
public class DictController {

	@Autowired
	private DictService dictService;

	@GetMapping("/all")
	@ResponseBody
    @ApiOperation(value = "获取所有角色", notes = "获取所有角色信息")//描述
	public Results<SysDict> getAll() {
		return dictService.getAllDicts();
	}

	@GetMapping("/list")
	@ResponseBody
    @PreAuthorize("hasAuthority('sys:dict:query')")
    @ApiOperation(value = "分页获取角色", notes = "用户分页获取角色信息")//描述
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "limit", required = true,dataType = "Integer"),
    })
	public Results list(PageTableRequest request) {
		log.info("DictController.list(): param ( request = " + request +" )");
		request.countOffset();
		return dictService.getAllDictsByPage(request.getOffset(), request.getLimit());
	}

    @GetMapping(value = "/add")
    @PreAuthorize("hasAuthority('sys:dict:add')")
    @ApiOperation(value = "新增角色信息页面", notes = "跳转到角色信息新增页面")//描述
    public String addDict(Model model) {
        model.addAttribute("sysDict",new SysDict());
        return "dict/dict-add";
    }

    @PostMapping(value = "/add")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:dict:add')")
    @ApiOperation(value = "保存角色信息", notes = "保存新增的角色信息")//描述
    @ApiImplicitParam(name = "dictDto",value = "角色信息实体类", required = true,dataType = "DictDto")
    public Results saveDict(@RequestBody SysDict dictDto) {
        return dictService.save(dictDto);
    }

    @GetMapping(value = "/edit")
    @ApiOperation(value = "编辑角色信息页面", notes = "跳转到角色信息编辑页面")//描述
    @ApiImplicitParam(name = "dict",value = "角色信息实体类", required = true,dataType = "SysDict")
    public String editDict(Model model, SysDict dict) {
        model.addAttribute("sysDict",dictService.getDictById(dict.getId()));
        return "dict/dict-edit";
    }

    @PostMapping(value = "/edit")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:dict:edit')")
    @ApiOperation(value = "保存角色信息", notes = "保存被编辑的角色信息")//描述
    @ApiImplicitParam(name = "dictDto",value = "角色信息实体类", required = true,dataType = "DictDto")
    public Results updateDict(@RequestBody SysDict dictDto) {
        return dictService.update(dictDto);
    }

    @GetMapping(value = "/delete")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:dict:del')")
    @ApiOperation(value = "删除角色信息", notes = "删除角色信息")//描述
    public Results<SysDict> deleteDict(SysDict dictDto) {
        return dictService.delete(dictDto.getId());
    }

    String pattern = "yyyy-MM-dd";
    //只需要加上下面这段即可，注意不能忘记注解
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));// CustomDateEditor为自定义日期编辑器
    }

    @GetMapping("/findDictByFuzzyDictName")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:dict:query')")
    @ApiOperation(value = "模糊查询角色信息", notes = "模糊搜索查询角色信息")//描述
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dictName",value = "模糊搜索的角色名", required = true),
    })
    public Results findDictByFuzzyDictName(PageTableRequest requests, String dictName) {
        requests.countOffset();
        return dictService.getDictByFuzzyDictNamePage(dictName,requests.getOffset(),requests.getLimit());
    }

    @GetMapping("/getDictByTypes")
    @ResponseBody
//    @PreAuthorize("hasAuthority('sys:dict:query')")
    public Results getDictByTypes(String dictType) {
        dictType = dictType.replace(" ", "");
        dictType = dictType.replace("=", "");
        dictType = dictType.replace(".", "");
        String[] types = dictType.split(",");
        return dictService.getDictByTypes(types);
    }

    @GetMapping("/getDictByType")
    @ResponseBody
//    @PreAuthorize("hasAuthority('sys:dict:query')")
    public Results getDictByType(String dictType) {
//        String[] types = dictType.split(",");
        return dictService.getDictByType(dictType);
    }
}
