package com.sxbang.friday.base.constant;

public enum PhysicalCondition {
    travel,
    contact,
    normal,
    rheum,
    polypnea,
    vomiting,
    flustered,
    conjunctivitis;
}
