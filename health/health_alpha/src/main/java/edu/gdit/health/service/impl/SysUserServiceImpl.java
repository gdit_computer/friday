package edu.gdit.health.service.impl;


import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.SysRoleUserDao;
import edu.gdit.health.dao.SysUserDao;
import edu.gdit.health.dto.SysUserDto;
import edu.gdit.health.model.SysRoleUser;
import edu.gdit.health.model.SysUser;
import edu.gdit.health.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class SysUserServiceImpl implements SysUserService {

    @Resource
    private SysUserDao sysUserDao;

    @Autowired
    private SysRoleUserDao sysRoleUserDao;

    @Override
    public SysUser getUser(String username) {
        return sysUserDao.getUser(username);
    }

    @Override
    public Results<SysUser> getAllUsersByPage(Integer startPosition, Integer limit) {
        return Results.success(sysUserDao.countAllUsers().intValue(),sysUserDao.getAllUsersByPage(startPosition,limit));
    }

    @Override
    public SysUser getUserByPhone(String phone) {
        return sysUserDao.getUserByPhone(phone);
    }

    @Override
    public SysUser getUserByEmail(String email) {
        return sysUserDao.getUserByEmail(email);
    }

    @Override
    public Results save(SysUser user,Long roleId) {

        if(roleId != null){
            sysUserDao.save(user);
            SysRoleUser sysRoleUser = new SysRoleUser();
            sysRoleUser.setRoleId(roleId);
            sysRoleUser.setUserId(user.getId());
            sysRoleUserDao.save(sysRoleUser);
            return Results.success();
        }
        return Results.failure();
    }

    @Override
    public SysUser getUserById(Long id) {
        return sysUserDao.getById(id);
    }

    @Override
    public Results updateUser(SysUserDto sysUserDto, Long roleId) {
        if(roleId != null){
            sysUserDao.updateUser(sysUserDto);
            SysRoleUser sysRoleUser = new SysRoleUser();
            sysRoleUser.setUserId(sysUserDto.getId());
            sysRoleUser.setRoleId(roleId);
            if(sysRoleUserDao.getSysRoleUserByUserId(sysUserDto.getId())!= null){
                sysRoleUserDao.updateSysRoleUser(sysRoleUser);
            }else{
                sysRoleUserDao.save(sysRoleUser);
            }
            return Results.success();
        }else{
            return Results.failure();
        }
    }

    public int deleteUser(Long id) {
        sysRoleUserDao.deleteRoleUserByUserId(id);
        return sysUserDao.deleteUser(id);
    }

    @Override
    public Results<SysUser> getUserByFuzzyUserNamePage(String username, Integer startPosition, Integer limit) {
        return Results.success(sysUserDao.getUserByFuzzyUserName(username).intValue(),sysUserDao.getUserByFuzzyUserNamePage(username,startPosition,limit));
    }

    @Override
    public Results<SysUser> changePassword(String username, String oldPassword, String newPassword) {
        SysUser u = sysUserDao.getUser(username);
        if (u == null) {
            return Results.failure(1,"用户不存在");
        }
        if (!new BCryptPasswordEncoder().encode(oldPassword).equals(u.getPassword())) {
            return Results.failure(1,"旧密码错误");
        }
        sysUserDao.changePassword(u.getId(), new BCryptPasswordEncoder().encode(newPassword));
        return Results.success();
    }
}
