package edu.gdit.health.service;

import edu.gdit.health.base.result.Results;

/**
 * 日志事件业务层
 */
public interface LoggingEventService {

    /**
     * 获取日志列表
     * @param offset 分页起始
     * @param limit  分页间隔
     * @return Results 返回分页结果集对象
     */
    Results getListByPage(Integer offset, Integer limit);
}
