package com.sxbang.friday.config;

import com.sxbang.friday.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

@Component
public class Scheduler {

    @Autowired
    private ReportService reportService;

    //每隔2秒执行一次
   /* @Scheduled(fixedRate = 2000)
    public void testTasks() {
        System.out.println("------ 定时任务执行时间：" + dateFormat.format(new Date()));
    }*/

    @Scheduled(fixedRate = 6000000)
    public void countReportData() {
        Date now = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println("------ 定时统计执行时间：" + sdf.format(now));
        reportService.countReportData(now);
    }
}
