package com.sxbang.friday.service.impl;

import com.sxbang.friday.base.result.ResponseCode;
import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.dao.DictDao;
import com.sxbang.friday.model.SysDict;
import com.sxbang.friday.service.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;

@Service
@Slf4j
public class DictServiceImpl implements DictService {

	@Resource
	private DictDao dictDao;


	@Override
	public Results<SysDict> getAllDicts() {
		return Results.success(50, dictDao.getAllDicts());
	}

	@Override
	public Results<SysDict> getAllDictsByPage(Integer offset, Integer limit) {
        return Results.success(dictDao.countAllDicts().intValue(), dictDao.getAllDictsByPage(offset, limit));
	}

    @Override
    public Results<SysDict> save(SysDict dictDto) {
        //1、先保存角色"
        dictDao.save(dictDto);
//        dictDao.saveDict(dictDto);
//        List<Long> permissionIds = dictDto.getPermissionIds();
        //移除0,permission id是从1开始
//        permissionIds.remove(0L);
        //2、保存角色对应的所有权限
//        if (!CollectionUtils.isEmpty(permissionIds)) {
//            dictPermissionDao.save(dictDto.getId(), permissionIds);
//        }
        return Results.success();
    }

    @Override
    public SysDict getDictById(Integer id) {
        return dictDao.getById(id);
    }

    @Override
    public Results update(SysDict dictDto) {
//        List<Long> permissionIds = dictDto.getPermissionIds();
//        permissionIds.remove(0L);
        //1、更新角色权限之前要删除该角色之前的所有权限
//        dictPermissionDao.deleteDictPermission(dictDto.getId());

        //2、判断该角色是否有赋予权限值，有就添加"
        /*if (!CollectionUtils.isEmpty(permissionIds)) {
            dictPermissionDao.save(dictDto.getId(), permissionIds);
        }*/

        //3、更新角色表
        int countData = dictDao.update(dictDto);

        if(countData > 0){
            return Results.success();
        }else{
            return Results.failure();
        }
    }

    @Override
    public Results delete(Integer id) {
//        List<SysDictUser> datas = dictUserDao.listAllSysDictUserByDictId(id);
//        if(datas.size() <= 0){
            dictDao.delete(id);
            return Results.success();
//        }
//        return Results.failure(ResponseCode.USERNAME_REPEAT.USER_ROLE_NO_CLEAR.getCode(),ResponseCode.USERNAME_REPEAT.USER_ROLE_NO_CLEAR.getMessage());
    }

    @Override
    public Results<SysDict> getDictByFuzzyDictNamePage(String dictName, Integer startPosition, Integer limit) {
        return Results.success(dictDao.countDictByFuzzyDictName(dictName).intValue(), dictDao.getDictByFuzzyDictNamePage(dictName,startPosition, limit));
    }

    @Override
    public Results getDictByTypes(String[] types) {
        return Results.success(getDictSet(types));
    }

    @Override
    public Results getDictByType(String types) {
        List<SysDict> list = dictDao.getDictInType(types);
        return Results.success(list);
    }

    @Override
    public Map<String, ArrayList> getDictSet(String[] types) {
        Map<String, ArrayList> rs = new HashMap<>();
        List<SysDict> list = dictDao.getDictInTypes(getQueryParam(types));
        for (SysDict dict : list) {
            if (!rs.containsKey(dict.getDictType())) {
                rs.put(dict.getDictType(), new ArrayList<>());
            }
            rs.get(dict.getDictType()).add(dict);
        }
        return rs;
    }

    private String getQueryParam(String[] types) {
        String param = String.join("','", types);
        param = "'" + param + "'";
        return param;
    }

}
