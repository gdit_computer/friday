package edu.gdit.health.service.impl;


import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.SysRoleUserDao;
import edu.gdit.health.model.SysRoleUser;
import edu.gdit.health.service.SysRoleUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysRoleUserServiceImpl implements SysRoleUserService {
    @Autowired
    private SysRoleUserDao sysRoleUserDao;

    @Override
    public Results getSysRoleUserByUserId(Long userId) {
        SysRoleUser sysRoleUser = sysRoleUserDao.getSysRoleUserByUserId(userId);
        if(sysRoleUser != null){
            return Results.success(sysRoleUser);
        }else{
            return Results.success();
        }
    }
}
