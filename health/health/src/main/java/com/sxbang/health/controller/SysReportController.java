package com.sxbang.health.controller;

import com.sxbang.health.base.result.Results;
import com.sxbang.health.model.SysReport;
import com.sxbang.health.service.SysReportService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Map;

@Controller
@RequestMapping("/report")
public class SysReportController {

    @Resource
    SysReportService sysReportService;

    @GetMapping(value = "add")
    public String addReport(Model model) {
        model.addAttribute("sysReport",new SysReport());
        Map<String, ArrayList> dict = sysReportService.getDictSet();
        model.addAttribute("reportDict", dict);
        return "report/report";
    }

    @PostMapping(value = "add")
    @ResponseBody
    public Results saveReport(@RequestBody SysReport report) {
        return sysReportService.save(report);
    }
}
