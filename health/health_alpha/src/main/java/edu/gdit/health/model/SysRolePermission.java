package edu.gdit.health.model;

import lombok.Data;

@Data
public class SysRolePermission {
    private Long roleId;
    private Long permissionId;
}
