package com.sxbang.health.controller;

import com.sxbang.health.base.result.Results;
import com.sxbang.health.model.SysReport;
import com.sxbang.health.service.SysReportService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;


@Controller
@RequestMapping("/report")
public class SysReportController {

    private static final Log log = LogFactory.getLog(SysReportController.class);

    @Resource
    SysReportService sysReportService;

    @GetMapping(value = "add")
    public String addReport(Model model) {
        log.warn("用户admin 进入数据采集页面。 ");
        model.addAttribute("sysReport",new SysReport());
        Map<String, ArrayList> dict = sysReportService.getDictSet();
        model.addAttribute("reportDict", dict);
        return "report/report";
    }

    @PostMapping(value = "add")
    @ResponseBody
    public Results saveReport(@RequestBody SysReport report) {
        return sysReportService.save(report);
    }

    @GetMapping(value = "/data")
    public String data(Model model) {
        log.warn("用户 %s 进入数据监控页面。");

        model.addAttribute("temperatureLine", sysReportService.countTemperatureLineData());
        model.addAttribute("temperature", sysReportService.countTemperatureData());
        model.addAttribute("travel", sysReportService.countTravelData());
        model.addAttribute("physicalCondition", sysReportService.countPhysicalConditionData());
        return "report/data";
    }

    @GetMapping(value = "chart/refresh")
    @ResponseBody
    public Results refresh() {
        return sysReportService.refresh();
    }

    @GetMapping(value = "chart/temperatureLine")
    @ResponseBody
    public Results temperatureLine(Date startDate, Date endDate, Integer past) {
        endDate = endDate == null ? new Date() : endDate;
        past = past == null ? 7 : past;
        if (startDate == null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(endDate);
            calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
            startDate = calendar.getTime();
        }
        return sysReportService.getChartTemperatureLine(startDate, endDate, past);
    }

    @GetMapping(value = "chart/temperature")
    @ResponseBody
    public Results chartTemperature(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return sysReportService.getChartTemperatureData(startDateStr, endDateStr);
    }

    @GetMapping(value = "chart/travel")
    @ResponseBody
    public Results chartTravel(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return sysReportService.getChartTravelData(startDateStr, endDateStr);
    }

    @GetMapping(value = "chart/physicalCondition")
    @ResponseBody
    public Results chartPhysicalCondition(Date startDate, Date endDate) {
        startDate = startDate == null ? new Date() : startDate;
        endDate = endDate == null ? startDate : endDate;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String startDateStr = sdf.format(startDate);
        String endDateStr = sdf.format(endDate);
        return sysReportService.getChartPhysicalConditionData(startDateStr, endDateStr);
    }

    String pattern = "yyyy-MM-dd";
    //只需要加上下面这段即可，注意不能忘记注解
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));// CustomDateEditor为自定义日期编辑器
    }

}
