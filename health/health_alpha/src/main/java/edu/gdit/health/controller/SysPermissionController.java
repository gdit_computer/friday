package edu.gdit.health.controller;

import com.alibaba.fastjson.JSONArray;

import edu.gdit.health.base.result.Results;
import edu.gdit.health.dto.SysRoleDto;
import edu.gdit.health.model.SysPermission;
import edu.gdit.health.service.SysPermissionService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * 权限控制器
 */
@Controller
@RequestMapping("permission")
@Slf4j
public class SysPermissionController {
    @Autowired
    private SysPermissionService sysPermissionService;

    /**
     * 获取所有菜单的权限值
     * @return Results 所有菜单的权限值
     */
    @RequestMapping(value = "/listAllPermission", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:query')")
    @ApiOperation(value = "获取所有权限值", notes = "获取所有菜单的权限值")//描述
    public Results<JSONArray> listAllPermission() {
        return sysPermissionService.listAllPermission();
    }

    /**
     * 根据角色Id去查询拥有的权限
     * @return Results 角色拥有的权限
     */
    @RequestMapping(value = "/listAllPermissionByRoleId", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "获取角色权限", notes = "根据角色Id去查询拥有的权限")//描述
    public Results<SysPermission> listAllPermissionByRoleId(SysRoleDto roleDto) {
        log.info(getClass().getName() + " : param =  " + roleDto);
        return sysPermissionService.listByRoleId(roleDto.getId());
    }

    /**
     * 获取所有菜单的权限值
     * @return Results 所有菜单的权限值
     */
    @GetMapping("/menuAll")
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:query')")
    @ApiOperation(value = "获取所有权限值", notes = "获取所有菜单的权限值")//描述
    public Results getMenuAll(){
        return sysPermissionService.getMenuAll();
    }

    /**
     * 跳转到菜单信息新增页面
     * @return String 所有菜单的权限值
     */
    @RequestMapping(value = "/add", method = RequestMethod.GET)
    @ApiOperation(value = "新增页面", notes = "跳转到菜单信息新增页面")//描述
    public String addPermission(Model model) {
        model.addAttribute("sysPermission",new SysPermission());
        return "permission/permission-add";
    }

    /**
     * 保存用户新增的菜单信息
     * @param permission 权限对象
     * @return Results
     */
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:add')")
    @ApiOperation(value = "添加菜单", notes = "保存用户新增的菜单信息")//描述
    @ApiImplicitParam(name = "sysPermission", value = "菜单权限实体sysPermission", required = true, dataType = "SysPermission")
    public Results<SysPermission> savePermission(@RequestBody SysPermission permission) {
        return sysPermissionService.save(permission);
    }

    /**
     * 跳转到菜单信息编辑页面
     * @param model
     * @param permission 权限对象
     * @return String
     */
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    @ApiOperation(value = "编辑页面", notes = "跳转到菜单信息编辑页面")//描述
    public String editPermission(Model model, SysPermission permission) {
        model.addAttribute("sysPermission",sysPermissionService.getSysPermissionById(permission.getId()));
        return "permission/permission-add";
    }

    /**
     * 保存用户编辑完的菜单信息
     * @param permission 权限对象
     * @return Results
     */
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:edit')")
    @ApiOperation(value = "更新菜单信息", notes = "保存用户编辑完的菜单信息")//描述
    @ApiImplicitParam(name = "sysPermission", value = "菜单权限实体sysPermission", required = true, dataType = "SysPermission")
    public Results updatePermission(@RequestBody SysPermission permission) {
        return sysPermissionService.updateSysPermission(permission);
    }


    /**
     * 根据菜单Id去删除菜单
     * @param sysPermission
     * @return Results
     */
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    @ResponseBody
    @PreAuthorize("hasAuthority('sys:menu:del')")
    @ApiOperation(value = "删除菜单", notes = "根据菜单Id去删除菜单")//描述
    public Results deletePermission(SysPermission sysPermission) {
        return sysPermissionService.delete(sysPermission.getId());
    }

    /**
     * "获取用户所属角色下能显示的菜单
     * @param userId
     * @return Results
     */
    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    @ResponseBody
    @ApiOperation(value = "获取菜单", notes = "获取用户所属角色下能显示的菜单")//描述
    @ApiImplicitParam(name = "userId", value = "userId", required = true, dataType = "Long")
    public Results<SysPermission> getMenu(Long userId) {
        return sysPermissionService.getMenu(userId);
    }

}
