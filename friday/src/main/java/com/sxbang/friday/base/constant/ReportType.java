package com.sxbang.friday.base.constant;

public enum ReportType {
    temperature,
    travel,
    physical_condition,
}
