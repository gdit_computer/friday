package com.sxbang.friday.dao;

import com.sxbang.friday.model.SysDict;
import com.sxbang.friday.model.SysReport;
import com.sxbang.friday.model.SysReportCount;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface ReportCountDao {

    @Select("select * from sys_report t")
    List<SysReport> getAllReports();

    @Select("select count(*) from sys_report t")
    Long countAllReports();

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_report_count(`name`, `value`, `type`, report_date, createTime) " +
            "values(#{name}, #{value}, #{type}, #{reportDate}, #{createTime})")
    int save(SysReportCount reportCount);

    @Select("select * from sys_report_count t where `type`=#{type} and DATE_FORMAT(t.createTime, '%Y-%m-%d') >= #{startDate}  and DATE_FORMAT(t.createTime, '%Y-%m-%d') <= #{endDate} ")
    List<SysReportCount> query(@Param("type")String type, @Param("startDate")String startDate, @Param("endDate")String endDate);

    @Delete(" delete from sys_report_count where report_date = #{reportDate} and `type`=#{type} and `name` = #{name} ")
    void delete(SysReportCount item);

    @Select("select ifnull(SUM(t.`value`), 0) from sys_report_count t where `type` = #{type} and `report_date` = #{startDate} and t.`name` in (${keys})")
    long sumValueByType(String type, String startDate, String keys);

}
