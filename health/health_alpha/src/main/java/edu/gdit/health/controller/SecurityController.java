package edu.gdit.health.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 安全控制器
 */
@Controller
public class SecurityController {

    /**
     * 由springmvc控制跳转到login页面
     * @return String 响应路径
     */
    @GetMapping(value = "/login.html")
    public String login() {
        return "login";
    }
    /**
     * 由springmvc控制跳转到403页面
     * @return String 响应路径
     */
    @GetMapping(value = "/403.html")
    public String noPermission() {
        return "403";
    }
}
