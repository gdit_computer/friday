package edu.gdit.health.service.impl;


import edu.gdit.health.base.result.Results;
import edu.gdit.health.dao.LoggingEventDao;
import edu.gdit.health.service.LoggingEventService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoggingEventServiceImpl implements LoggingEventService {

    @Resource
    private LoggingEventDao loggingEventDao;

    @Override
    public Results getListByPage(Integer offset, Integer limit) {
        return Results.success(loggingEventDao.countAll().intValue(), loggingEventDao.getListByPage(offset, limit));
    }
}
