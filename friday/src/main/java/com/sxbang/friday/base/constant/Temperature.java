package com.sxbang.friday.base.constant;

public enum Temperature {
    NORMAL,
    LOW,
    MIDDLE,
    HIGH
}
