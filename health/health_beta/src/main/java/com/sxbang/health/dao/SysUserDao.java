package com.sxbang.health.dao;

import com.sxbang.health.model.SysPermission;
import com.sxbang.health.model.SysUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface SysUserDao {

    @Select("select * from sys_user t where t.username = #{username}")
    SysUser getUser(String username);
}
