package com.sxbang.friday.dao;

import com.sxbang.friday.model.SysDict;
import com.sxbang.friday.model.SysRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DictDao {

    @Select("select * from sys_dict t")
    List<SysDict> getAllDicts();

    @Select("select count(*) from sys_dict t")
    Long countAllDicts();

    @Select("select * from sys_dict t limit #{startPosition}, #{limit}")
    List<SysDict> getAllDictsByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_dict(dict_key, dict_value, dict_type, remark) values(#{dictKey}, #{dictValue}, #{dictType}, #{remark})")
    int save(SysDict dict);

//    int saveDict(SysDict dict);

    @Select("select * from sys_dict t where t.id = #{id}")
    SysDict getById(Integer id);

    int update(SysDict dict);

    @Delete("delete from sys_dict where id = #{id}")
    int delete(Integer id);

    @Select("select count(*) from sys_dict t where t.name like '%${dictName}%'")
    Long countDictByFuzzyDictName(@Param("dictName") String dictName);

    @Select("select * from sys_dict t where t.name like '%${dictName}%' limit #{startPosition},#{limit}")
    List<SysDict> getDictByFuzzyDictNamePage(@Param("dictName") String dictName,@Param("startPosition")Integer startPosition,@Param("limit")Integer limit);

//    @Select("select * from sys_dict t where t.dict_type in #{types}")
    @Select("select * from sys_dict t where t.dict_type in (${types})")
    List<SysDict> getDictInTypes(@Param("types") String types);

    @Select("select * from sys_dict t where t.dict_type = #{type}")
    List<SysDict> getDictInType(@Param("type") String type);
}
