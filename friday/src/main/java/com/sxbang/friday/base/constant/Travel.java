package com.sxbang.friday.base.constant;

public enum Travel {

    YES(1),
    NO(0);

    private final int num;

    Travel(int num){
        this.num = num;
    }
}