package com.sxbang.friday.controller;

import com.sxbang.friday.base.result.PageTableRequest;
import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.model.LoggingEvent;
import com.sxbang.friday.model.SysDict;
import com.sxbang.friday.service.DictService;
import com.sxbang.friday.service.LoggingEventService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("log")
@Slf4j
public class LoggingEventController {

	@Autowired
	private LoggingEventService loggingEventService;

	@GetMapping("/all")
	@ResponseBody
    @ApiOperation(value = "获取所有角色", notes = "获取所有角色信息")//描述
	public Results<LoggingEvent> getAll() {
		return loggingEventService.getAll();
	}

	@GetMapping("/list")
	@ResponseBody
    @PreAuthorize("hasAuthority('sys:dict:query')")
    @ApiOperation(value = "分页获取", notes = "分页获取")//描述
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "limit", required = true,dataType = "Integer"),
    })
	public Results list(PageTableRequest request) {
		log.info("LoggingEventController.list(): param ( request = " + request +" )");
		request.countOffset();
		return loggingEventService.getAllByPage(request.getOffset(), request.getLimit());
	}

    String pattern = "yyyy-MM-dd";
    //只需要加上下面这段即可，注意不能忘记注解
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));// CustomDateEditor为自定义日期编辑器
    }

}
