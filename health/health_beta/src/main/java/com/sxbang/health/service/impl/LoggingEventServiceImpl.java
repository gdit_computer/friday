package com.sxbang.health.service.impl;

import com.sxbang.health.base.result.Results;
import com.sxbang.health.dao.LoggingEventDao;
import com.sxbang.health.service.LoggingEventService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class LoggingEventServiceImpl  implements LoggingEventService {

    @Resource
    private LoggingEventDao loggingEventDao;

    @Override
    public Results getListByPage(Integer offset, Integer limit) {
        return Results.success(loggingEventDao.countAll().intValue(), loggingEventDao.getListByPage(offset, limit));
    }
}
