package com.sxbang.health.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/api")
public class ApiController {

    private static final Log log = LogFactory.getLog(ApiController.class);

    @ResponseBody
    @GetMapping("test")
    public String test(String name) {
        return "test/" + name;
    }

    @RequestMapping(value="/getPage")
    public ModelAndView getPage(ModelAndView modelAndView, String pageName){
        pageName = null == pageName ? "index" : pageName;
        log.warn(String.format("用户打开了新页面[%s]。", pageName));
        modelAndView.setViewName(pageName);
        return modelAndView;
    }
}
