package com.sxbang.friday.service.impl;

import com.sxbang.friday.base.result.Results;
import com.sxbang.friday.dao.DictDao;
import com.sxbang.friday.dao.LoggingEventDao;
import com.sxbang.friday.model.LoggingEvent;
import com.sxbang.friday.model.SysDict;
import com.sxbang.friday.service.DictService;
import com.sxbang.friday.service.LoggingEventService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class LoggingEventServiceImpl implements LoggingEventService {

	@Resource
	private LoggingEventDao loggingEventDao;


	@Override
	public Results getAll() {
		return Results.success(50, loggingEventDao.getAll());
	}

	@Override
	public Results getAllByPage(Integer offset, Integer limit) {
        return Results.success(loggingEventDao.countAll().intValue(), loggingEventDao.getAllByPage(offset, limit));
	}

    @Override
    public LoggingEvent getById(Integer id) {
        return loggingEventDao.getById(id);
    }

}
