package edu.gdit.health.service;



import edu.gdit.health.base.result.Results;
import edu.gdit.health.model.SysReport;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public interface SysReportService {

    /**
     * 获取词典集合
     * @return Map<String, ArrayList>
     */
    Map<String, ArrayList> getDictSet();

    /**
     * 增加数据采集信息
     * @return Map<String, ArrayList>
     */
    Results save(SysReport report);

    /**
     * 统计近7天体温趋势总记录数
     * @return
     */
    long countTemperatureLineData();

    /**
     * 统计当日体温数据总记录数
     * @return
     */
    long countTemperatureData();

    /**
     * 统计当日旅行数据总记录数
     * @return
     */
    long countTravelData();

    /**
     * 统计当日症状数据总记录数
     * @return
     */
    long countPhysicalConditionData();

    /**
     * 统计采集信息总记录数
     * @return
     */
    void countReportData(Date now);

    /**
     * 用户分页获取数据采集信息
     * @param offset
     * @param limit
     * @return Results
     */
    Results getSelfReportsByPage(Integer offset, Integer limit);

    /**
     * 刷新数据监控业务
     * @return Results
     */
    Results refresh();

    /**
     * 近7天体温趋势
     * @param startDate
     * @param endDate
     * @param past 体温趋势日期范围,为null时默认为7天
     * @return
     */
    Results getChartTemperatureLine(Date startDate, Date endDate, Integer past);

    /**
     * 当日体温数据
     * @param startDateStr
     * @param endDateStr
     * @return
     */
    Results getChartTemperatureData(String startDateStr, String endDateStr);

    /**
     * 当日旅行数据
     * @param startDateStr
     * @param endDateStr
     * @return
     */
    Results getChartTravelData(String startDateStr, String endDateStr);

    /**
     * 当日症状数据
     * @param startDateStr
     * @param endDateStr
     * @return
     */
    Results getChartPhysicalConditionData(String startDateStr, String endDateStr);

}
