package edu.gdit.health.dao;

import edu.gdit.health.model.SysRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * 角色持久层
 */
@Mapper
public interface SysRoleDao {

    /**
     * 获取所有角色信息
     * @return List<SysRole>
     */
    @Select("select * from sys_role t")
    List<SysRole> getAllRoles();

    /**
     * 统计所有角色数量
     * @return Long
     */
    @Select("select count(*) from sys_role t")
    Long countAllRoles();

    /**
     * 查询用户角色列表（分页）
     * @param startPosition
     * @param limit
     * @return  List<SysRole>
     */
    @Select("select * from sys_role t limit #{startPosition}, #{limit}")
    List<SysRole> getAllRolesByPage(@Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

    /**
     * 保存角色信息
     * @param role
     * @return int
     */
    @Options(useGeneratedKeys = true, keyProperty = "id")
    @Insert("insert into sys_role(name, description, createTime, updateTime) values(#{name}, #{description}, now(), now())")
    int save(SysRole role);

//    int saveRole(SysRole role);

    /**
     * 根据id查询角色信息
     * @param id
     * @return SysRole
     */
    @Select("select * from sys_role t where t.id = #{id}")
    SysRole getById(Long id);

    /**
     * 更新角色信息
     * @param role
     * @return int
     */
    int update(SysRole role);

    /**
     * 删除角色信息
     * @param id
     * @return int
     */
    @Delete("delete from sys_role where id = #{id}")
    int delete(Long id);

    /**
     * 统计模糊搜索查询角色信息记录总数
     * @param roleName
     * @return
     */
    @Select("select count(*) from sys_role t where t.name like '%${roleName}%'")
    Long countRoleByFuzzyRoleName(@Param("roleName") String roleName);

    /**
     * 模糊搜索查询角色信息并分页
     * @param roleName
     * @param startPosition
     * @param limit
     * @return
     */
    @Select("select * from sys_role t where t.name like '%${roleName}%' limit #{startPosition},#{limit}")
    List<SysRole> getRoleByFuzzyRoleNamePage(@Param("roleName") String roleName, @Param("startPosition")Integer startPosition, @Param("limit")Integer limit);

}
