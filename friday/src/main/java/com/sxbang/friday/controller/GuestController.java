package com.sxbang.friday.controller;

import com.sxbang.friday.model.SysReport;
import com.sxbang.friday.service.DictService;
import com.sxbang.friday.util.SecurityUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.Map;

@Slf4j
@Controller
public class GuestController {

    @Autowired
    private DictService dictService;

    @GetMapping("report")
    public String reportIndex(Model model) {
        log.warn("游客进入数据采集页面。");
        model.addAttribute("sysReport",new SysReport());
        Map<String, ArrayList> dict = dictService.getDictSet(new String[]{"temperature" ,"travel" ,"physical_condition" });
        model.addAttribute("reportDict", dict);
        return "home/report";
    }

    @GetMapping("data")
    public String reportData() {
        log.warn("游客进入数据监控页面。");
        return "home/data";
    }
}
