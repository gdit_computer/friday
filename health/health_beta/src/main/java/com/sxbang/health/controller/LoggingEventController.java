package com.sxbang.health.controller;

import com.sxbang.health.base.result.PageTableRequest;
import com.sxbang.health.base.result.Results;
import com.sxbang.health.service.LoggingEventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("/log")
public class LoggingEventController {

    @Autowired
    private LoggingEventService loggingEventService;

    @GetMapping("/list")
    @ResponseBody
    public Results list(PageTableRequest request) {
        request.countOffset();
        return loggingEventService.getListByPage(request.getOffset(), request.getLimit());
    }

    String pattern = "yyyy-MM-dd";
    //只需要加上下面这段即可，注意不能忘记注解
    @InitBinder
    public void initBinder(WebDataBinder binder, WebRequest request) {
        binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat(pattern), true));// CustomDateEditor为自定义日期编辑器
    }
}
